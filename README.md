# DigitizerClass2

FESA class to manage gnuradio-flowgraphs with Picotec soft oscilloscopes (Digitizers) inside

Channel data is served in a way to fulfill the [DAQ API](https://git.acc.gsi.de/fesa-classes/daq_api)
 
Please check the related [GSI Wiki page](https://www-acc.gsi.de/wiki/FESA/FESA-Class-Digitizer) for more information
