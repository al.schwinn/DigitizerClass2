
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _DigitizerClass2_CheckCPUandRamAction_H_
#define _DigitizerClass2_CheckCPUandRamAction_H_

#include <DigitizerClass2/GeneratedCode/Device.h>
#include <DigitizerClass2/GeneratedCode/GenRTActions.h>
#include <DigitizerClass2/RealTime/UserCode/cpu-stat/CPUSnapshot.h>

namespace DigitizerClass2
{

class CheckCPUandRamAction : public CheckCPUandRamActionBase
{
public:
    CheckCPUandRamAction (fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~CheckCPUandRamAction();
    void execute(fesa::RTEvent* pEvt);

    CPUSnapshot latestCPUSnapshot_;

    // number of times the workload limit for a specific CPU was exceeded
    int32_t cpu_limit_exceeded[MAX_NUMBER_OF_CPUS];

    // number of times the workload limit for virtual ram was exceeded
    int32_t ram_limit_exceeded;

};

} // DigitizerClass2

#endif // _DigitizerClass2_CheckCPUandRamAction_H_
