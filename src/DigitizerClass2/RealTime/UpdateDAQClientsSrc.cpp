
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <DigitizerClass2/RealTime/UpdateDAQClientsSrc.h>
#include <DigitizerClass2/RealTime/RTDeviceClass.h>
#include <DigitizerClass2/Common/CabadTimingContext.h>
#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>

#include <fesa-core/Synchronization/NoneContext.h>
#include <fesa-core/RealTime/AbstractEventSourceFactory.h>


#include <cmw-log/Logger.h>

namespace
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.RealTime.UpdateDAQClientsSrc");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::event); \
    diagMsg.fesaClass = "DigitizerClass2"; \
    diagMsg.name = "UpdateDAQClientsSrc"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    DigitizerClass2ServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

namespace DigitizerClass2
{

UpdateDAQClientsSrc* UpdateDAQClientsSrc::self_ = NULL;


UpdateDAQClientsSrc::UpdateDAQClientsSrc(const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses) :
    UpdateDAQClientsSrcBase ("UpdateDAQClientsSrc", serviceLocator, serviceLocatorRelatedClasses)
{

}

UpdateDAQClientsSrc::~UpdateDAQClientsSrc()
{
}

// static method
void UpdateDAQClientsSrc::cb_data_ready(uint32_t data_id)
{
    if(self_ != NULL)
        self_->wakeup(data_id);
}

void UpdateDAQClientsSrc::wakeup(uint32_t data_id)
{
    std::lock_guard<std::mutex> lock(conditionMutex_);
    data_id_queue_.push(data_id);
    condVar_.notify_all(); // set condition to true;
}

void UpdateDAQClientsSrc::connect(const boost::shared_ptr<fesa::EventElement>& eventElement)
{
    if(self_ == NULL)
        self_ = dynamic_cast<UpdateDAQClientsSrc*>(fesa::AbstractEventSourceFactory::getEventSource("DigitizerClass2", "UpdateDAQClientsSrc"));
}

void UpdateDAQClientsSrc::wait(boost::shared_ptr<fesa::RTEvent>& eventToFire)
{
    uint32_t data_id = 0;
    {
        std::unique_lock<std::mutex> lock(conditionMutex_);
        if(data_id_queue_.empty())
        {
            // Wait till condition is signaled
            condVar_.wait(lock);
        }
        data_id = data_id_queue_.front();
        data_id_queue_.pop();
    }

    cabad::ClientNotificationData data;
    bool found = RTDeviceClass::getInstance()->getDAQDataReadyManager()->getDataByID(data_id, data);
    if(!found)
        throw fesa::FesaException(__FILE__, __LINE__, "Failed to find notificationData for data_id: " + std::to_string(data_id));


    //std::cout << "UpdateTriggeredClientsSrc::wait - data_id: " << std::to_string(data_id) << std::endl;
    boost::shared_ptr<RTEventPayload> payload(new fesa::RTEventPayload(std::to_string(data_id)));
    eventToFire->setPayload(payload);

    boost::shared_ptr<MultiplexingContext> multiplexingContext;
    if(data.context_ != NULL)
    {
        std::shared_ptr<const CabadTimingContext> cabad_timing_context;
        if( !( cabad_timing_context = std::dynamic_pointer_cast<const CabadTimingContext>(data.context_)))
        {
            LOG_ERROR_IF(logger, "failed to cast cabad::TimingContext to CabadTimingContext");
            return;
        }
        multiplexingContext = cabad_timing_context->getWRContext();
    }
    else
    {
        multiplexingContext.reset(new fesa::NoneContext);
    }
    createEvent(eventToFire, UpdateDAQClientsSrc::defaultEvent, multiplexingContext);
    // Once this method returns, the fesa-core will process our event and post it to all interested schedulers.
    // After that, this method will be called again.
}

} // DigitizerClass2
