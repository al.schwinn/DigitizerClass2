/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#ifndef _DigitizerClass2_CalcTimeOffsetAction_H_
#define _DigitizerClass2_CalcTimeOffsetAction_H_

#include <DigitizerClass2/GeneratedCode/Device.h>
#include <DigitizerClass2/GeneratedCode/GenRTActions.h>
#include <cabad/Util.h>
#include <TimingReceiver.h>

namespace DigitizerClass2
{

class CalcTimeOffsetAction : public CalcTimeOffsetActionBase
{
public:
    CalcTimeOffsetAction (fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~CalcTimeOffsetAction();
    void execute(fesa::RTEvent* pEvt);

    void measureTimeToGetWRStamp();
    void measureTimeToGetSystemStamp();

private:
    int64_t readTimingReceiverClockOffset();
    std::shared_ptr<saftlib::TimingReceiver_Proxy> timingReceiver_;

    cabad::average_filter<double> last_clock_offsets_;

    int64_t timeToGetSystemStamp_ns_;
    int64_t timeToGetWRStamp_ns;
};

} // DigitizerClass2

#endif // _DigitizerClass2_CalcTimeOffsetAction_H_
