/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#ifndef _DigitizerClass2_DigitizerTriggerAction_H_
#define _DigitizerClass2_DigitizerTriggerAction_H_

#include <DigitizerClass2/GeneratedCode/Device.h>
#include <DigitizerClass2/GeneratedCode/GenRTActions.h>

#include <fesa-core-gsi/Synchronization/TimingContextWR.h>
#include <cabad/TimingContext.h>

namespace DigitizerClass2
{

class DigitizerTriggerAction : public DigitizerTriggerActionBase
{
public:
    DigitizerTriggerAction (fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~DigitizerTriggerAction();
    void execute(fesa::RTEvent* pEvt);
};

} // DigitizerClass2

#endif // _DigitizerClass2_DigitizerTriggerAction_H_
