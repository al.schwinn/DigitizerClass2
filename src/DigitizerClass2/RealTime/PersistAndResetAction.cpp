
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>
#include <DigitizerClass2/RealTime/PersistAndResetAction.h>

#include <DigitizerClass2/RealTime/RTDeviceClass.h>

#include <cmw-log/Logger.h>

namespace
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.RealTime.PersistAndResetAction");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "DigitizerClass2"; \
    diagMsg.name = "PersistAndResetAction"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    DigitizerClass2ServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace DigitizerClass2
{

PersistAndResetAction::PersistAndResetAction(fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses) :
     PersistAndResetActionBase(rtActionConfig, serviceLocator, serviceLocatorRelatedClasses) 
{
}

PersistAndResetAction::~PersistAndResetAction()
{
}

// This method is called whenever an event triggers this action; this is where custom code must be written.
// A real time action operates on a device collection, this is why a loop which iterates over the device collection
// of this real time action is there.
void PersistAndResetAction::execute(fesa::RTEvent* pEvt)
{
    const Devices& devices = getFilteredDeviceCollection(pEvt);

    // Wait a seconds to make sure settings from 'Init' are done'
    sleep(1);

    // store the new flowgraph for the next stratup
    this->serviceLocator_->triggerPersistency();

    // close USB connection to all digitizers
    RTDeviceClass::getInstance()->closeDigitizerConnections();

    // Give the persistancy a second to do the persist
    sleep(1);

    // Currently only Hard reset ... later on we might try to e.g. reload the flowgraph
    std::cout << "-- Killing process DigitizerDU2 --" << std::endl;
    system("killall -9 DigitizerDU2");
}

} // DigitizerClass2
