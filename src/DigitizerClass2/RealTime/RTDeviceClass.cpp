/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <DigitizerClass2/RealTime/RTDeviceClass.h>
#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>
#include <DigitizerClass2/Common/FlowGraphLoader.h>
#include <DigitizerClass2/Common/DeviceDataBuffer.h>
#include <DigitizerClass2/Common/Sink.h>

#include <fesa-core/Core/AbstractEvent.h>
#include <fesa-core/Synchronization/NoneContext.h>
#include <fesa-core/Utilities/StringUtilities.h>
#include <fesa-core-gsi/RealTime/TimingUtility.h>
#include <fesa-core-gsi/Synchronization/WRTimingDefs.h>
#include <fesa-core-gsi/Synchronization/TimingContextWR.h>

#include <cabad/Util.h>

#include <cmw-log/Logger.h>

#include <string>
#include <vector>

#include <gnuradio/constants.h> // version
#include <digitizers/constants.h> // version
#include <flowgraph/constants.h> // version

#include <volk/volk_prefs.h>

#include <cmath>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <utility>
#include <stdexcept>
#include <limits>
#include <unistd.h>
#include <stdexcept>

// saftlib
#include <SAFTd.h>
#include <TimingReceiver.h>
#include <SoftwareActionSink.h>
#include <SoftwareCondition.h>
#include <iDevice.h>
#include <Output.h>
#include <Input.h>
#include <OutputCondition.h>
#include "../Common/FlowGraphLoader.h"

namespace
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.RealTime.RTDeviceClass");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "DigitizerClass2"; \
    diagMsg.name = "RTDeviceClass"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    RTDeviceClass::logDiagnosticMessage(topic, diagMsg); \
}

namespace DigitizerClass2
{

// Rate limiting for circular buffer log messages
time_t last_log_stamp_sec;
bool log_rate_limiting_active = false;
int skipped_log_messages = 0;

cmw::log::Level::LogLevel cabadLogLevel2cmwLogLevel(cabad::LogLevel logLevel)
{
    if      (logLevel == cabad::LogLevel::ERROR)
        return cmw::log::Level::LL_ERROR;
    else if (logLevel == cabad::LogLevel::WARNING)
        return cmw::log::Level::LL_WARNING;
    else if (logLevel == cabad::LogLevel::INFO)
        return cmw::log::Level::LL_INFO;
    else if (logLevel == cabad::LogLevel::TRACE)
        return cmw::log::Level::LL_TRACE;
    else if (logLevel == cabad::LogLevel::DEBUG)
        return cmw::log::Level::LL_DEBUG;
    else
        throw std::invalid_argument("Unknown cabad log level");
}

bool cabad_debug = false;

void circular_buffer_log_function(cabad::LogLevel severity, const std::string& file, int line, const std::string& message, const std::string& topic)
{
    if(topic.empty())
    {
        LOG_DIAG_IF("GENERAL", message);
    }
    else
    {
        std::cout << file  << ":" << line << " Message: " << message << std::endl;
        LOG_DIAG_IF("PER_CHANNEL", message);
    }

    if(cabad_debug)
    {
        std::cout << file  << ":" << line << " Message: " << message << std::endl;
        return;
    }

    if(!logger.isLoggable(cabadLogLevel2cmwLogLevel(severity)))
        return;

    struct timespec spec;
    clock_gettime(CLOCK_REALTIME, &spec);

    // For ERROR and WARNING we use rate limiting (max one message/sec) to dont flood greylog on accident (TRACE/INFO, etc anyhow is disabled by default)
    if((severity == cabad::LogLevel::ERROR || severity == cabad::LogLevel::WARNING) && last_log_stamp_sec == spec.tv_sec )
    {
        log_rate_limiting_active = true;
        skipped_log_messages++;
        return; // ignore this log message
    }

    last_log_stamp_sec = spec.tv_sec;
    std::string full_message;
    if(log_rate_limiting_active)
      full_message = "Rate limiting for circular buffer library skipped " + std::to_string(skipped_log_messages)+ " log messages. --";
    skipped_log_messages = 0;
    log_rate_limiting_active = false;

    full_message += file  + std::string(":") + std::to_string(line) + " Message: " + message;

    if      (severity == cabad::LogLevel::ERROR)
    {
        LOG_ERROR_IF(logger, full_message);
    }
    else if (severity == cabad::LogLevel::WARNING)
    {
        LOG_WARNING_IF(logger, full_message);
    }
    else if (severity == cabad::LogLevel::INFO)
    {
        LOG_INFO_IF(logger, full_message);
    }
    else if (severity == cabad::LogLevel::TRACE)
    {
        LOG_TRACE_IF(logger, full_message);
    }
    else if (severity == cabad::LogLevel::DEBUG)
    {
        LOG_DEBUG_IF(logger, full_message);
    }
    else
    {
        LOG_ERROR_IF(logger, "Unknown circular buffer log level" );
    }

//    if(full_message.find("GS11MU2:Current_Set@25Hz") != std::string::npos ||
//       full_message.find("GS11MU2:Voltage_Set@25Hz") != std::string::npos)
//    {
//        std::cout << "*************** Halt condition found --- exiting ***************" << std::endl;
//        exit(1);
//    }
}

RTDeviceClass* RTDeviceClass::instance_ = NULL;

RTDeviceClass::RTDeviceClass() :
                RTDeviceClassGen(),
                utc_offset_(0)
{
    std::size_t bufferSize = this->DigitizerClass2ServiceLocator_->getGlobalDevice()->contextTrackerBufferSize.get();
    contextTracker_.reset(new cabad::ContextTracker(bufferSize));

    daqDataReadyManager_.reset(new cabad::DataReadyManager(UpdateDAQClientsSrc::cb_data_ready, // Called when data of some circular buffers is ready to be shipped to clients
                                                          1000));                               // Maximum number of Data-Ready Notifications to queue up (e.g. relevant for slow clients)

    flowGraphLoaded_ = false;
    mod_status_pexaria_ = MODULE_STATUS::UNKNOWN;
    mod_status_picoscopes_ = MODULE_STATUS::OK; // TODO: Check scope status, temperature, etc periodically

    class_start_time_ = clock_.now();
}

RTDeviceClass::~RTDeviceClass()
{
    const Devices& deviceCol = DigitizerClass2ServiceLocator_->getDeviceCollection();
    for (auto device : deviceCol )
    {
        try
        {
            FlowGraphLoader::getInstance().stopFlowGraph (device);
        }
        catch(const std::exception& exception)
        {
            LOG_ERROR_IF(logger, exception.what());
        }
    }
}

RTDeviceClass* RTDeviceClass::getInstance()
{
    if (instance_ == NULL)
    {
        instance_ = new RTDeviceClass();
    }
    return instance_;
}

void RTDeviceClass::releaseInstance()
{
    if (instance_ != NULL)
    {
        delete instance_;
        instance_ = NULL;
    }
}

void RTDeviceClass::logDiagnosticMessage(const std::string& topic, fesa::DiagnosticsDefs::DiagnosticMessage& diagMsg)
{
    RTDeviceClass::getInstance()->getServiceLocator()->logDiagnosticMessage(topic, diagMsg);
}

std::shared_ptr<saftlib::Output_Proxy> getOutputProxy(const std::string &devName, const std::string &ioName)
{
    auto devices = saftlib::SAFTd_Proxy::create()->getDevices();
    if (devices.empty())
    {
        throw fesa::FesaException(__FILE__, __LINE__, "no timing device present");
    }

    auto it = devices.find(devName);
    if (it == devices.end())
    {
        throw fesa::FesaException(__FILE__, __LINE__, "can't find timing device: " + devName);
    }

    auto receiver = saftlib::TimingReceiver_Proxy::create(it->second);
    auto outputs = receiver->getOutputs();

    auto output_it = outputs.find(ioName);
    if (output_it == outputs.end())
    {
        throw fesa::FesaException(__FILE__, __LINE__, "can't find IO " + ioName);
    }

    return saftlib::Output_Proxy::create(output_it->second);
}

void RTDeviceClass::configureOutput(const std::string &devName, const std::string &ioName)
{
    auto output_proxy = getOutputProxy(devName, ioName);

    output_proxy->setOutputEnable(true);
    output_proxy->WriteOutput(false);
}

void RTDeviceClass::createOutputCondition(const std::string &devName, const std::string &ioName, guint64 eventID, guint64 eventMask, gint64 offset, bool ioEdge)
{
    auto output_proxy = getOutputProxy(devName, ioName);

    // Setup condition
    auto condition = saftlib::OutputCondition_Proxy::create(output_proxy->NewCondition(true, eventID, eventMask, offset, ioEdge));

    condition->setAcceptConflict(false);
    condition->setAcceptDelayed(true);
    condition->setAcceptEarly(true);
    condition->setAcceptLate(true);
}

void RTDeviceClass::destroyOutputConditions(const std::string &devName, const std::string &ioName)
{
    auto output_proxy = getOutputProxy(devName, ioName);

    for (auto &cname : output_proxy->getActiveConditions())
    {
        auto condition = saftlib::OutputCondition_Proxy::create(cname);
        if (condition->getDestructible())
            condition->Destroy();
    }
}

// Timing is optional therefore the below code is enclosed in the try/catch block. Note
// if an exception is thrown (meaning there is no timing device present, configuration
// is invalid, ...)

// Note, at startup all output conditions for a given output are destroyed (well at least
// those marked as destructible or without the owner). It is assumed that we own
// a given IO on the timing card.
void RTDeviceClass::configurePexariaIOs (Device *device, std::string ftrnName)
{
    std::cout << "## Starting configuration of pexaria IO's" << std::endl;

    try
    {
        auto eventsNames = device->getInstantiationData()->getConcreteEventNames("digitizerTriggerEvent");

        std::string ioNames_raw = device->ftrnIoName.getAsString();

        std::vector<std::string> io_names;
        fesa::StringUtilities::tokenize(ioNames_raw, io_names, ",");

        auto pulseWidthNanoseconds = static_cast<int64_t>(1000000000.0 * device->pulseWidth.get());

        auto groupId = fesaGSI::extractTimingGroupFromTimingDomain(device->getInstantiationData()->getTimingDomain());

        for (auto io_name : io_names)
        {
            LOG_INFO_IF(logger, "Timing device: " + ftrnName + ", IO name: " + io_name + ", pulse width: " + std::to_string(pulseWidthNanoseconds) + "ns" + ", GID: " + std::to_string(groupId));

            destroyOutputConditions(ftrnName, io_name);
            configureOutput(ftrnName, io_name);

            for (const auto &eventName : eventsNames)
            {
                boost::shared_ptr<DeviceDataBuffer> buffer = getDeviceDataBuffer(device);
                auto eventNumber = fesaGSI::extractEventID(eventName);

                if(buffer->isTriggerEventEnabled(eventNumber) == false)
                    continue;

                auto mask = fesaGSI::generate_event_id_mask(groupId, eventNumber);
                auto event = fesaGSI::generate_event_id(groupId, eventNumber);

                std::cout << "Creating output conditions on pexaria IO: '" << io_name << "' for event "<< eventName << std::endl;
                // Rising edge
                createOutputCondition(ftrnName, io_name, event, mask, 0, true);
                // Falling edge
                createOutputCondition(ftrnName, io_name, event, mask, pulseWidthNanoseconds, false);
            }
        }
    }
    catch(const fesa::FesaException& exception)
    {
        LOG_ERROR_IF(logger, exception.getMessage());
        // keep the process running, just log the error
    }

    std::cout << "## Configuration of pexaria IO's finished" << std::endl;
}
void reportToErrorCollection(Device *device, const std::exception& exception)
{
    NoneContext context;
    std::string message = exception.what();

    // Split into multiple because of stupid limitattion of GSI Error Collection
    std::size_t limit = 256;
    for (std::size_t i = 0; i < message.size(); i+=limit)
    {
        std::string submessage;
        if( i+limit <  message.size())
            submessage = message.substr (i,i+limit-1);
        else
            submessage = message.substr (i, message.size() - 1);
        device->error_collection.addError(4711,submessage,&context,device);
    }

    LOG_ERROR_IF(logger, exception.what());
}

void RTDeviceClass::specificInit()
{
    NoneContext context;
    const Devices& deviceCol = DigitizerClass2ServiceLocator_->getDeviceCollection();
    auto globalDev = DigitizerClass2ServiceLocator_->getGlobalDevice();

    // init fields which are not updated often, in order to prevent warning on early subscribe
    globalDev->offset_utc_wr_2_utc_system.set(0, &context);

    // Configure timing card
    std::string ftrnName = globalDev->ftrnDeviceName.getAsString();

    cabad::Events::initEvents(257, 258);

    for (auto device : deviceCol )
    {
        try
        {
            // Status & state
            device->powerState.set(DEVICE_POWER_STATE::ON, &context);
            device->control.set(DEVICE_CONTROL::REMOTE, &context);
            device->status.set(DEVICE_STATUS::ERROR, &context);
            device->modulesReady.set(false, &context);
            device->opReady.set(false, &context);
            device->interlock.set(false, &context);
            device->detailedStatus.set("flowGraphLoaded", false, &context);
            device->detailedStatus.set("sampleRateValid", true, &context);

            device->moduleStatus.set("Picoscopes", mod_status_picoscopes_, &context);
            device->moduleStatus.set("Pexaria", mod_status_pexaria_, &context);

            std::vector<std::string> events = device->getInstantiationData()->getConcreteEventNames(LOGICAL_EVENT_NAME_FOR_DIGITIZER_TRIGGERS);
            boost::shared_ptr<DeviceDataBuffer> buffer(new DeviceDataBuffer(events,
                                                                            device->getName(),
                                                                            device->refTriggerEvent.get(&context),
                                                                            device->refTriggerFallbackEvent1.get(&context),
                                                                            device->refTriggerFallbackEvent2.get(&context) ));

            // Disable all events which where disabled before (info from persistancy file)
            for ( auto triggerName : device->disabledTriggerEvents.getStrings(&context) )
            {
                if(buffer->isTriggerEvent(triggerName))
                    buffer->setTriggerEventEnableState(buffer->eventName2eventID(triggerName), false);
            }

            deviceDataCol_[device->getName()] = buffer;

            FlowGraphLoader::getInstance().loadFlowgraph (device);
            buffer->setTriggerMatchingTolerance(device->triggerMatchingTolerance.get(&context));

            // only ready, if no execption while loading flowgraph
            flowGraphLoaded_ = true;
            device->detailedStatus.set("flowGraphLoaded", flowGraphLoaded_, &context);
            device->modulesReady.set(true, &context);
            device->opReady.set(true, &context);
            device->status.set(DEVICE_STATUS::OK, &context);

            LOG_INFO_IF(logger, "Configuring timing device: " + ftrnName);
            configurePexariaIOs (device, ftrnName);
        }
        catch(const std::exception& exception)
        {
            reportToErrorCollection (device, exception);
            // Start Fesa class even with corrupt flowgraph. (Flowgraph can be reset by calling 'Init')
        }
    }

    // set a log function for the circular buffer
    cabad::setLogFunction(circular_buffer_log_function);

    char volk_config_path[1000];
    volk_get_config_path(&volk_config_path[0]);
    if(volk_config_path[0] == 0)
    {
        std::cout << "no volk_config found .. exiting " << std::endl;
        exit (1);
    }
    else
      std::cout << "volk_config loaded from here: " << volk_config_path << std::endl;

    for (auto device : deviceCol )
    {
        try
        {

            FlowGraphLoader::getInstance().startFlowGraph (device);
        }
        catch(const std::exception& exception)
        {
            reportToErrorCollection (device, exception);
            // Start Fesa class even with corrupt flowgraph. (Flowgraph can be reset by calling 'Init')
        }
    }

//    // used for debugging
//    forAllSinks([&](Sink* sink) {
//            CircularBufferManager* bufferManager = dynamic_cast<CircularBufferManager*>(sink->getCircularBufferManager());
//
//            if(bufferManager->getSignalName() == "GSI123XY4:SPARE_ChannelD@1Hz" || bufferManager->getSignalName() == "GSI123XY4:SPARE_ChannelE@1Hz")
//                {
//                    std::cout << "setting log-level of signal '" << bufferManager->getSignalName() << "' to TRACE." << std::endl;
//                    bufferManager->setTypeLogLevel(cabad::ClientNotificationType::FULL_SEQUENCE, cabad::LogLevel::TRACE);
//                }
//    });

    std::cout << "RTDeviceClass::specificInit finished!" << std::endl;

}


void RTDeviceClass::closeDigitizerConnections()
{
    std::cout << "-- Shuting down all Digitizers --" << std::endl;

    const Devices& deviceCol = DigitizerClass2ServiceLocator_->getDeviceCollection();
    for (auto device : deviceCol )
    {
        auto flowgraph = getDeviceDataBuffer(device)->getFlowGraph();
        flowgraph->digitizers_apply(
            [&](const std::string &name, gr::digitizers::digitizer_block *digitizer)
        {
            digitizer->close();
        });
    }
}

// This method is executed just before a normal shut down of the process.
void RTDeviceClass::specificShutDown()
{
    closeDigitizerConnections();

    std::cout << "-- Killing process DigitizerDU2 --" << std::endl;

    // Fesa stole the possibility to use "exit" here. As long as we are not able to properly shutdown the flowgraph, we will rely on killall
    system("killall -9 DigitizerDU2");
    return;

//TODO: Proper Shutdown
//    const Devices& deviceCol = DigitizerClass2ServiceLocator_->getDeviceCollection();
//    for (auto device : deviceCol )
//    {
//        try
//        {
//            std::cout << "unloading flowgraph of device " << device->getName() << std::endl;
//            boost::shared_ptr<DeviceDataBuffer> buffer = getDeviceDataBuffer(device);
//            FlowGraphLoader::unload (buffer.get());
//            std::cout << "unloading done ... exiting now " << std::endl;
//        }
//        catch(const std::exception& exception)
//        {
//            LOG_ERROR_IF(logger, exception.what());
//        }
//    }
}

void RTDeviceClass::forAllSinks(std::function<void(Sink*)> const &func)
{
    for (auto &deviceData : deviceDataCol_)
    {
        deviceData.second->forAllSinks(func);
    }
}

boost::shared_ptr<DeviceDataBuffer> RTDeviceClass::getDeviceDataBuffer(Device* pDev)
{
    auto iter = deviceDataCol_.find(pDev->getName());
    if( iter == deviceDataCol_.end() )
        throw fesa::FesaException(__FILE__, __LINE__, "No deviceData available for device '" + pDev->getName() + "'");
    return iter->second;
}

Sink* RTDeviceClass::getSink(const Device* pDev, std::string sinkID)
{
    auto iter = deviceDataCol_.find(pDev->getName());
    if( iter == deviceDataCol_.end() )
        throw fesa::FesaException(__FILE__, __LINE__, "No deviceData available for device '" + pDev->getName() + "'");
    return iter->second->getSinkById(sinkID);
}

int32_t RTDeviceClass::getUptimeInSeconds() const
{
    std::chrono::duration<int32_t> uptime = std::chrono::duration_cast<std::chrono::seconds> (clock_.now() - class_start_time_);
    return uptime.count();
}

bool RTDeviceClass::resyncTriggerTags()
{
    std::lock_guard<std::mutex> lock(resyncTriggerTagsMutex_);

    if(resyncTriggerTags_ == false)
        return false;

    std::chrono::duration<int32_t> resync_time = std::chrono::duration_cast<std::chrono::seconds> (clock_.now() - resyncTriggerTagsTime_);
    if(resync_time.count() > 10) // 10 sec for a resync
    {
        resyncTriggerTags_ = false;
        return false;
    }

    return true;
}

void RTDeviceClass::requestResyncTriggerTags()
{
    std::lock_guard<std::mutex> lock(resyncTriggerTagsMutex_);

    if(resyncTriggerTags_ == false)
    {
        resyncTriggerTagsTime_ = clock_.now();
        resyncTriggerTags_ = true;
    }
}

} // DigitizerClass2
