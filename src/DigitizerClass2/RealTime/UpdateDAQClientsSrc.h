
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _DigitizerClass2_UpdateDAQClientsSrc_H_
#define _DigitizerClass2_UpdateDAQClientsSrc_H_

#include <fesa-core/RealTime/AbstractEventSource.h>
#include <fesa-core/RealTime/RTEvent.h>
#include <fesa-core/Synchronization/MultiplexingContext.h>
#include <fesa-core/Utilities/Mutex.h>
#include <fesa-core/Utilities/ConditionalVariable.h>

#include <fesa-core-gsi/Synchronization/TimingContextWR.h>

#include <DigitizerClass2/GeneratedCode/CustomEventSourceGen.h>

#include <cabad/CircularBufferManagerBase.h>

#include <condition_variable>
#include <queue>

namespace DigitizerClass2
{

class UpdateDAQClientsSrc : public UpdateDAQClientsSrcBase
{
public:
    // Will be called by cabad::DataReadyManager whenever there is new data available
    static void cb_data_ready(uint32_t data_id);
    void wakeup(uint32_t data_id);
private:
    friend class EventSourceFactory;
    UpdateDAQClientsSrc (const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual  ~UpdateDAQClientsSrc();
    void wait(boost::shared_ptr<fesa::RTEvent>& eventToFire);
    void connect(const boost::shared_ptr<fesa::EventElement>& eventElement);

    std::queue< uint32_t> data_id_queue_;

    std::condition_variable condVar_;
    std::mutex conditionMutex_;

    static UpdateDAQClientsSrc* self_;
};

} // DigitizerClass2

#endif // _DigitizerClass2_UpdateDAQClientsSrc_H_
