
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>
#include <DigitizerClass2/RealTime/CheckCPUandRamAction.h>

#include <cmw-log/Logger.h>

#include "sys/types.h"
#include "sys/sysinfo.h"


namespace
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.RealTime.CheckCPUandRamAction");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "DigitizerClass2"; \
    diagMsg.name = "CheckCPUandRamAction"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    DigitizerClass2ServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace DigitizerClass2
{

// Number of times the CPU / Ram workload limit has to be exceeded before restart
#define N_LIMIT_EXCEEDED_BEFORE_RESTART 5
#define CPU_WORKLOAD_LIMIT_PERCENT 95
#define RAM_WORKLOAD_LIMIT_PERCENT 95

CheckCPUandRamAction::CheckCPUandRamAction(fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses) :
     CheckCPUandRamActionBase(rtActionConfig, serviceLocator, serviceLocatorRelatedClasses) 
{
    ram_limit_exceeded = 0;
    for (std::size_t i = 0; i < MAX_NUMBER_OF_CPUS; i++)
        cpu_limit_exceeded[i] = 0;
}

CheckCPUandRamAction::~CheckCPUandRamAction()
{
}

int parseLine(char* line){
    // This assumes that a digit will be found and the line ends in " Kb".
    int i = strlen(line);
    const char* p = line;
    while (*p <'0' || *p > '9') p++;
    line[i-3] = '\0';
    i = atoi(p);
    return i;
}

int getValue(){
    FILE* file = fopen("/proc/self/status", "r");
    int result = -1;
    char line[128];

    while (fgets(line, 128, file) != NULL){
        if (strncmp(line, "VmSize:", 7) == 0){
            result = parseLine(line);
            break;
        }
    }
    fclose(file);
    return result;
}


void CheckCPUandRamAction::execute(fesa::RTEvent* pEvt)
{
    // ####### CPU #######
    CPUSnapshot newCPUSnapshot_;
    if (newCPUSnapshot_.GetNumEntries() != latestCPUSnapshot_.GetNumEntries())
    {
        LOG_ERROR_IF(logger, "Number of detected CPU's changed .. one of them just burned ?? :F ");
        return;
    }

    int32_t cpu_active_percent[MAX_NUMBER_OF_CPUS];
    std::size_t i = 0;
    for (; i < newCPUSnapshot_.GetNumEntries(); i++)
    {
        const float ACTIVE_TIME     = newCPUSnapshot_.GetActiveTime(i) - latestCPUSnapshot_.GetActiveTime(i);
        const float IDLE_TIME       = newCPUSnapshot_.GetIdleTime(i) - latestCPUSnapshot_.GetIdleTime(i);
        const float TOTAL_TIME      = ACTIVE_TIME + IDLE_TIME;

        cpu_active_percent[i] = int32_t(100.f * ACTIVE_TIME / TOTAL_TIME);
        //std::cout << "cpu #" << i << " - "  << active_percent[i] << "%"<< std::endl;
    }

    this->DigitizerClass2ServiceLocator_->getGlobalDevice()->CPU_load_percent.set(cpu_active_percent, i, pEvt->getMultiplexingContext());

    latestCPUSnapshot_ = newCPUSnapshot_;

    // ####### Memory #######
    struct sysinfo memInfo;

    sysinfo (&memInfo);
    long long totalVirtualMem = memInfo.totalram;
    //Add other values in next statement to avoid int overflow on right hand side...
    totalVirtualMem += memInfo.totalswap;
    totalVirtualMem *= memInfo.mem_unit;
    //std::cout << "totalVirtualMem [kiB]: " << totalVirtualMem << std::endl;

    long long virtualMemProcess = getValue();
    int32_t virtualMemProcess_percent = (virtualMemProcess * 100) / (totalVirtualMem / 1024);
    //std::cout << "virtualMemProcess [byte]: " << virtualMemProcess << std::endl;
    //std::cout << "virtualMemProcess %: " << virtualMemProcess_percent << std::endl;

    // Use that part to see if the numbers above make sense (application should exit aproximatly at 100 %VSZ)
//    size_t bytes = 1024 * 1024 * 1024; // 1GB
//    char * buffer = (char*)malloc (bytes);
//    if (buffer==NULL) exit (1);
//    for (size_t n=0; n<bytes; n++)
//       buffer[n]=rand()%26+'a';

    this->DigitizerClass2ServiceLocator_->getGlobalDevice()->virtual_memory_used_percent.set(virtualMemProcess_percent, pEvt->getMultiplexingContext());

    // ### check if we are stuck / a reset is required ####
    if(this->DigitizerClass2ServiceLocator_->getGlobalDevice()->cpu_ram_watchdog_enabled.get(pEvt->getMultiplexingContext()))
    {
        bool reset_ram_required = false;
        bool reset_cpu_required = false;
        for (i = 0; i < newCPUSnapshot_.GetNumEntries(); i++)
        {
            if( cpu_active_percent[i] > CPU_WORKLOAD_LIMIT_PERCENT)
            {
                cpu_limit_exceeded[i] ++;
                if (cpu_limit_exceeded[i] > N_LIMIT_EXCEEDED_BEFORE_RESTART)
                    reset_cpu_required = true;
            }
            else
            {
                cpu_limit_exceeded[i] = 0;
            }
        }
        if( virtualMemProcess_percent > RAM_WORKLOAD_LIMIT_PERCENT )
        {
            ram_limit_exceeded ++;
            if( ram_limit_exceeded > N_LIMIT_EXCEEDED_BEFORE_RESTART)
                reset_ram_required = true;
        }
        else
        {
            ram_limit_exceeded = 0;
        }

        if(reset_cpu_required || reset_ram_required)
        {
            std::ostringstream message;
            if(reset_cpu_required)
                message << "At least one CPU was on" << CPU_WORKLOAD_LIMIT_PERCENT << "% for a period of " << N_LIMIT_EXCEEDED_BEFORE_RESTART << " seconds";
            else
                message << "Virtual RAM usage was above " << RAM_WORKLOAD_LIMIT_PERCENT << "% for a period of " << N_LIMIT_EXCEEDED_BEFORE_RESTART << " seconds";

            message << " Resetting FESA class .. ";
            LOG_WARNING_IF(logger, message.str());

            const Devices& devices = getFilteredDeviceCollection(pEvt);
            for (Devices::const_iterator it = devices.begin(); it != devices.end(); ++it)
            {
                Device* device = *it;
                // reset persistant flowgraph and author, and trigger the reset
                device->flowGraph.set("",pEvt->getMultiplexingContext());
                device->lastFlowGraphEditor.set("",pEvt->getMultiplexingContext());
                AbstractAction::triggerOnDemandEventSource("PersistAndResetSource",pEvt->getMultiplexingContext());
            }
        }
    }
}


} // DigitizerClass2
