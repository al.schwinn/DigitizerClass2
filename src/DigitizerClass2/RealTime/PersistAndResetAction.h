
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _DigitizerClass2_PersistAndResetAction_H_
#define _DigitizerClass2_PersistAndResetAction_H_

#include <DigitizerClass2/GeneratedCode/Device.h>
#include <DigitizerClass2/GeneratedCode/GenRTActions.h>

namespace DigitizerClass2
{

class PersistAndResetAction : public PersistAndResetActionBase
{
public:
    PersistAndResetAction (fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~PersistAndResetAction();
    void execute(fesa::RTEvent* pEvt);
};

} // DigitizerClass2

#endif // _DigitizerClass2_PersistAndResetAction_H_
