/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>
#include <DigitizerClass2/RealTime/CalcTimeOffsetAction.h>
#include <DigitizerClass2/RealTime/RTDeviceClass.h>

#include <cmw-log/Logger.h>
#include <stdexcept>

#include <SAFTd.h>
#include <TimingReceiver.h>
#include <SoftwareActionSink.h>
#include <SoftwareCondition.h>
#include <SCUbusCondition.h>
#include <SCUbusActionSink.h>

namespace
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.RealTime.CalcTimeOffsetAction");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "DigitizerClass2"; \
    diagMsg.name = "CalcTimeOffsetAction"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    DigitizerClass2ServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace DigitizerClass2
{

static int hasLock = FALSE;

static void on_locked_callback(bool locked)
{
  if (locked) {
    hasLock = TRUE;
    LOG_ERROR_IF(logger, "Timing receiver regained its lock");
    //std::cout << "WR Locked!" << std::endl;
  } else {
    hasLock = FALSE;
    LOG_ERROR_IF(logger, "Timing receiver lost its lock. Not possible to measure White Rabbit clock offset");
    //std::cout << "WR Lost Lock!" << std::endl;
  }
}

void CalcTimeOffsetAction::measureTimeToGetWRStamp()
{
    saftlib::Time tr_time1 = timingReceiver_->CurrentTime();
    saftlib::Time tr_time2 = timingReceiver_->CurrentTime();
    timeToGetWRStamp_ns = tr_time2.getUTC() - tr_time1.getUTC();
    //std::cout << "timeToGetWRStamp_ns     : " << timeToGetWRStamp_ns << std::endl;
    // here some measurements for wr: 25280, 22688, 22448, 22112, 102833, 105719 ns
}

void CalcTimeOffsetAction::measureTimeToGetSystemStamp()
{
    timespec system_time1, system_time2;
    clock_gettime(CLOCK_REALTIME, &system_time1);
    clock_gettime(CLOCK_REALTIME, &system_time2);
    uint64_t linux_time1 = (system_time1.tv_sec * 1000000000) + (system_time1.tv_nsec);
    uint64_t linux_time2 = (system_time2.tv_sec * 1000000000) + (system_time2.tv_nsec);
    timeToGetSystemStamp_ns_ = linux_time2 - linux_time1;
    // std::cout << "timeToGetSystemStamp_ns_     : " << timeToGetSystemStamp_ns_ << std::endl;
    // Here some mesurement for the system: 64, 64, 31, 26 ns
}

CalcTimeOffsetAction::CalcTimeOffsetAction(fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses) :
     CalcTimeOffsetActionBase(rtActionConfig, serviceLocator, serviceLocatorRelatedClasses), last_clock_offsets_(10)
{
    auto timingDevices = saftlib::SAFTd_Proxy::create()->getDevices();
    // Create a proxy object in this process that has access to
    // the remote object stored inside the saftd daemon.

   std::string timingReceiverName = DigitizerClass2ServiceLocator_->getGlobalDevice()->ftrnDeviceName.getAsString();
   timingReceiver_ = saftlib::TimingReceiver_Proxy::create(timingDevices[timingReceiverName.c_str()]);

   if(timingReceiver_->getLocked())
   {
       hasLock = TRUE;
   }
   else
   {
       LOG_ERROR_IF(logger, "Timing receiver is running without lock. Not possible to measure White Rabbit clock offset");
       hasLock = FALSE;
   }

   std::cout << "-- Waiting for UTC offset initialization ..." << std::endl;

   // Run a callback when lock is lost
   timingReceiver_->Locked.connect(sigc::ptr_fun(&on_locked_callback));
}

CalcTimeOffsetAction::~CalcTimeOffsetAction()
{
}

// White Rabbit UTC Time and System UTC time will differ up to 5ms in the worst case (user net)
// In order to  we will need to add the offset to each gnuradio tag timestamp
int64_t CalcTimeOffsetAction::readTimingReceiverClockOffset()
{
    NoneContext context;
    timespec linux_time_spec;

    // First we update our estimates (possibly dependend on system load)
    measureTimeToGetWRStamp();
    measureTimeToGetSystemStamp();

    clock_gettime(CLOCK_REALTIME, &linux_time_spec);

    saftlib::Time tr_time = timingReceiver_->CurrentTime();
    uint64_t linux_time_utc_clean = (linux_time_spec.tv_sec * 1000000000) + (linux_time_spec.tv_nsec) - timeToGetSystemStamp_ns_;
    uint64_t wr_time_utc_clean = tr_time.getUTC() - timeToGetWRStamp_ns;
    // std::cout << "TR Offset: " << tr_time << " took " << (linux_time2 - linux_time1) << std::endl;
    // will be delayed by 1 dbus call

    // assume start->end id 2 dbus calls
//    std::cout << "linux_time_utc_clean     : " << linux_time_utc_clean << std::endl;
//    std::cout << "   wr_time_utc_clean     : " << wr_time_utc_clean << std::endl;

    //std::cout << "Offset UTC WR - UTC System - ns: " << int64_t(wr_time_utc_clean - linux_time_utc_clean) << std::endl;

    double offset_in_seconds = double(int64_t(wr_time_utc_clean - linux_time_utc_clean)) / 1.e9;
    last_clock_offsets_.push(offset_in_seconds);
    std::cout << "Offset UTC WR - UTC System - [s]: " << last_clock_offsets_.get_average() << std::endl;
    this->DigitizerClass2ServiceLocator_->getGlobalDevice()->offset_utc_wr_2_utc_system.set(last_clock_offsets_.get_average(), &context);

    return (int64_t(wr_time_utc_clean - linux_time_utc_clean));
}

// This method is called whenever an event triggers this action; this is where custom code must be written.
// A real time action operates on a device collection, this is why a loop which iterates over the device collection
// of this real time action is there.
void CalcTimeOffsetAction::execute(fesa::RTEvent* pEvt)
{
    try
    {
        if(!hasLock)
        {
            for (auto const& device : getFilteredDeviceCollection(pEvt) )
                RTDeviceClass::getInstance()->mod_status_pexaria_ = MODULE_STATUS::ERROR;
            LOG_ERROR_IF(logger, "Timing receiver is running without lock. Not possible to measure White Rabbit clock offset");
            return;
        }
        for (auto const& device : getFilteredDeviceCollection(pEvt) )
            RTDeviceClass::getInstance()->mod_status_pexaria_ = MODULE_STATUS::OK;
        RTDeviceClass::getInstance()->set_utc_offset(readTimingReceiverClockOffset());
    }
    catch (const fesa::FesaException& exception)
    {
        LOG_ERROR_IF(logger, exception.getMessage());
    }
    catch (std::exception& ex)
    {
        LOG_ERROR_IF(logger, ex.what());
    }
    catch (...)
    {
        LOG_ERROR_IF(logger, "Unknown Exception in CalcTimeOffsetAction::execute");
    }
}

} // DigitizerClass2
