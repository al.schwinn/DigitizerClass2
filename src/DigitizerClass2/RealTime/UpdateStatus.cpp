
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>
#include <DigitizerClass2/RealTime/UpdateStatus.h>
#include <DigitizerClass2/Common/Sink.h>
#include <DigitizerClass2/Common/Definitions.h>
#include <cmw-log/Logger.h>

namespace
{

static int triggers_not_matched_count = 0;

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.RealTime.UpdateStatus");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "DigitizerClass2"; \
    diagMsg.name = "UpdateStatus"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    DigitizerClass2ServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace DigitizerClass2
{

UpdateStatus::UpdateStatus(fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses) :
     UpdateStatusBase(rtActionConfig, serviceLocator, serviceLocatorRelatedClasses) 
{
    rtDeviceClass_ = RTDeviceClass::getInstance();
}

UpdateStatus::~UpdateStatus()
{
}

// This method is called whenever an event triggers this action; this is where custom code must be written.
// A real time action operates on a device collection, this is why a loop which iterates over the device collection
// of this real time action is there.
void UpdateStatus::execute(fesa::RTEvent* pEvt)
{
    const fesa::MultiplexingContext* context = pEvt->getMultiplexingContext();
    static_cast<void>(context); // This line prevents an "unused variable" warning, it can be removed safely.
    const Devices& devices = getFilteredDeviceCollection(pEvt);
    if (devices.size() != 1)
    {
        LOG_ERROR_IF(logger, "Digitizer class only works with a single device");
        return;
    }
    Devices::const_iterator device = devices.begin();

    bool triggers_not_matched = false;
    try
    {
        int index = 0;
        rtDeviceClass_->forAllSinks([&](Sink* sink) {
                CircularBufferManager* bufferManager = dynamic_cast<CircularBufferManager*>(sink->getCircularBufferManager());
                double matched_triggers_percent = bufferManager->getMatchedTriggersPercentage();

                if( matched_triggers_percent < TRIGGER_MATCHING_PERCENTAGE_MIN)
                    triggers_not_matched = true;

                (*device)->matchedTriggersPerChannel_percentage.setCell(matched_triggers_percent, index,pEvt->getMultiplexingContext());
                index++;
        });

        if(triggers_not_matched)
        {
            LOG_WARNING_IF(logger, "To few matching timing triggers on device '" + (*device)->getName() + "''. At least " + std::to_string(TRIGGER_MATCHING_PERCENTAGE_MIN) + "% of incoming triggers have to match!"
                                   " Try to increase the sample rate of the scope, decrease the bloc-size or modify the trigger matching tolerance!");
            triggers_not_matched_count++;
            if(triggers_not_matched_count > 10)
            {
                LOG_ERROR_IF(logger, "For some reason the trigger matching fails. WR-Events can no longer be matched to the incoming data stream. Possibly selected trigger-events are to close to each other. Starting Resync Procedure." );
                rtDeviceClass_->requestResyncTriggerTags();
                rtDeviceClass_->getContextTracker()->clear();
                triggers_not_matched_count = 0;
            }
        }

        (*device)->powerState.set(DEVICE_POWER_STATE::UNKNOWN, pEvt->getMultiplexingContext());
        (*device)->interlock.set(false, pEvt->getMultiplexingContext());

        int32_t seconds_total = RTDeviceClass::getInstance()->getUptimeInSeconds();
        int32_t minutes_total = seconds_total / 60;
        int32_t hours_total = minutes_total / 60;

        int32_t seconds = seconds_total % 60;
        int32_t minutes = minutes_total % 60;
        int32_t hours = hours_total % 24;
        int32_t days = hours_total / 24;

        std::string uptime = std::to_string(days) + "d:" + std::to_string(hours) + "h:" + std::to_string(minutes) + "m:" + std::to_string(seconds) + "s";
        this->DigitizerClass2ServiceLocator_->getGlobalDevice()->uptime.set(uptime.c_str(), pEvt->getMultiplexingContext());

//        bool detailedStatus[2] = {false, false};
//        (*device)->detailedStatus.set(detailedStatus,0,pEvt->getMultiplexingContext());

        // TODO: Update picoscope status (e.g. use temperature sensors)
        (*device)->moduleStatus.set("Picoscopes", RTDeviceClass::getInstance()->mod_status_picoscopes_, pEvt->getMultiplexingContext());
        (*device)->moduleStatus.set("Pexaria", RTDeviceClass::getInstance()->mod_status_pexaria_, pEvt->getMultiplexingContext());

        (*device)->detailedStatus.set("sampleRateValid",         rtDeviceClass_->sampleRateValid_, context);
        (*device)->detailedStatus.set("flowGraphLoaded",         rtDeviceClass_->flowGraphLoaded_, context);
        (*device)->detailedStatus.set("triggerTagsSynchronized", !rtDeviceClass_->resyncTriggerTags(), context);

        if( rtDeviceClass_->mod_status_pexaria_ == MODULE_STATUS::OK &&
            rtDeviceClass_->mod_status_picoscopes_ == MODULE_STATUS::OK &&
            rtDeviceClass_->flowGraphLoaded_ == true &&
            rtDeviceClass_->sampleRateValid_ == true &&
            rtDeviceClass_->resyncTriggerTags() == false)
        {
            (*device)->modulesReady.set(true, pEvt->getMultiplexingContext());
            (*device)->opReady.set(true, pEvt->getMultiplexingContext());
            (*device)->status.set(DEVICE_STATUS::OK, pEvt->getMultiplexingContext());
        }
        else
        {
            (*device)->modulesReady.set(false, pEvt->getMultiplexingContext());
            (*device)->opReady.set(false, pEvt->getMultiplexingContext());
            (*device)->status.set(DEVICE_STATUS::ERROR, pEvt->getMultiplexingContext());
        }
    }
    catch (const fesa::FesaException& exception)
    {
        LOG_ERROR_IF(logger, exception.getMessage());
    }
    catch (const std::exception& ex)
    {
        LOG_ERROR_IF(logger, ex.what());
    }
    catch(...)
    {
        LOG_ERROR_IF(logger, "Unknown Exception in UpdateStatus::execute");
    }
}

} // DigitizerClass2
