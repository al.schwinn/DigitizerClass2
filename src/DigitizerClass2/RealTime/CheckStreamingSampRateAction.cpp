
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>
#include <DigitizerClass2/RealTime/CheckStreamingSampRateAction.h>
#include <DigitizerClass2/RealTime/RTDeviceClass.h>
#include <DigitizerClass2/Common/Sink.h>
#include <DigitizerClass2/Common/Definitions.h>

#include <cmw-log/Logger.h>

namespace
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.RealTime.CheckStreamingSampRateAction");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "DigitizerClass2"; \
    diagMsg.name = "CheckStreamingSampRateAction"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    DigitizerClass2ServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace DigitizerClass2
{

CheckStreamingSampRateAction::CheckStreamingSampRateAction(fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses) :
     CheckStreamingSampRateActionBase(rtActionConfig, serviceLocator, serviceLocatorRelatedClasses) 
{
    rtDeviceClass_ = RTDeviceClass::getInstance();
}

CheckStreamingSampRateAction::~CheckStreamingSampRateAction()
{
}

// This method is called whenever an event triggers this action; this is where custom code must be written.
// A real time action operates on a device collection, this is why a loop which iterates over the device collection
// of this real time action is there.
void CheckStreamingSampRateAction::execute(fesa::RTEvent* pEvt)
{
    const fesa::MultiplexingContext* context = pEvt->getMultiplexingContext();
    const Devices& devices = getFilteredDeviceCollection(pEvt);
    if (devices.size() != 1)
    {
        LOG_ERROR_IF(logger, "Digitizer class only works with a single device");
        return;
    }
    Devices::const_iterator device = devices.begin();

    timespec system_time1;
    clock_gettime(CLOCK_REALTIME, &system_time1);
    uint64_t now_ns = (system_time1.tv_sec * 1000000000) + (system_time1.tv_nsec);

    std::vector<std::string> sinkNames;
    std::string channel_errors;

    // [&] captures all local variables by reference
    rtDeviceClass_->forAllSinks([&](Sink* sink) {
        // Only log the first error
        if(sink->getSinkMode() == SinkMode::Streaming)
        {
            CircularBufferManager* bufferManager = dynamic_cast<CircularBufferManager*>(sink->getCircularBufferManager());
            channel_errors += bufferManager->checkSampRate(sink->getSampleRate(), now_ns);
        }
    });

    if(!channel_errors.empty())
    {
        RTDeviceClass::getInstance()->sampleRateValid_ = false;
        std::string message = "The measured sample rate for the following channels do not match the expectations:\n" + channel_errors;
        (*device)->error_collection.addError(4711,message,context,(*device));
    }
    else
        RTDeviceClass::getInstance()->sampleRateValid_ = true;
}

} // DigitizerClass2
