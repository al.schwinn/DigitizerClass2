
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _DigitizerClass2_CheckStreamingSampRateAction_H_
#define _DigitizerClass2_CheckStreamingSampRateAction_H_

#include <DigitizerClass2/GeneratedCode/Device.h>
#include <DigitizerClass2/GeneratedCode/GenRTActions.h>

namespace DigitizerClass2
{

class RTDeviceClass;

class CheckStreamingSampRateAction : public CheckStreamingSampRateActionBase
{
public:
    CheckStreamingSampRateAction (fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~CheckStreamingSampRateAction();
    void execute(fesa::RTEvent* pEvt);

private:
    RTDeviceClass* rtDeviceClass_;
};

} // DigitizerClass2

#endif // _DigitizerClass2_CheckStreamingSampRateAction_H_
