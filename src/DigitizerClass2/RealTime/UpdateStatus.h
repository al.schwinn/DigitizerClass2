
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _DigitizerClass2_UpdateStatus_H_
#define _DigitizerClass2_UpdateStatus_H_

#include <DigitizerClass2/GeneratedCode/Device.h>
#include <DigitizerClass2/GeneratedCode/GenRTActions.h>
#include <DigitizerClass2/RealTime/RTDeviceClass.h>

namespace DigitizerClass2
{

class UpdateStatus : public UpdateStatusBase
{
public:
    UpdateStatus (fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~UpdateStatus();
    void execute(fesa::RTEvent* pEvt);

private:
    RTDeviceClass* rtDeviceClass_;
};

} // DigitizerClass2

#endif // _DigitizerClass2_UpdateStatus_H_
