/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>
#include <DigitizerClass2/RealTime/DigitizerTriggerAction.h>
#include <DigitizerClass2/RealTime/RTDeviceClass.h>
#include <DigitizerClass2/Common/DeviceDataBuffer.h>
#include <DigitizerClass2/Common/Definitions.h>
#include <DigitizerClass2/Common/CabadTimingContext.h>

#include <cabad/ContextTracker.h>

#include <cmw-log/Logger.h>

#include <fesa-core-gsi/Utilities/Saftlib.h>

#include <SAFTd.h>
#include <TimingReceiver.h>
#include <SoftwareActionSink.h>
#include <SoftwareCondition.h>
#include <SCUbusCondition.h>
#include <SCUbusActionSink.h>

namespace
{

static int64_t trigger_tag_count = 0;

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.RealTime.DigitizerTriggerAction");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "DigitizerClass2"; \
    diagMsg.name = "DigitizerTriggerAction"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    DigitizerClass2ServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace DigitizerClass2
{

DigitizerTriggerAction::DigitizerTriggerAction(fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses) :
                DigitizerTriggerActionBase(rtActionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

DigitizerTriggerAction::~DigitizerTriggerAction()
{
}

uint64_t get_timestamp_nano_utc()
{
    timespec start_time;
    clock_gettime(CLOCK_REALTIME, &start_time);
    return (start_time.tv_sec * 1000000000) + (start_time.tv_nsec);
}

uint64_t get_timestamp_milli_utc()
{
    return uint64_t( get_timestamp_nano_utc() / 1000000 );
}

// This method is called whenever an event triggers this action; this is where custom code must be written.
// A real time action operates on a device collection, this is why a loop which iterates over the device collection
// of this real time action is there.
void DigitizerTriggerAction::execute(fesa::RTEvent* pEvt)
{
    try
    {
        RTDeviceClass* rtDeviceClass = RTDeviceClass::getInstance();

        // Dont do anything during resync, just reset the counter
        if(rtDeviceClass->resyncTriggerTags() == true)
        {
            trigger_tag_count = 0;
            return;
        }

        boost::shared_ptr<fesaGSI::TimingContextWR> contextWR;
        if (! (contextWR = boost::dynamic_pointer_cast<fesaGSI::TimingContextWR>(pEvt->getMultiplexingContextSharedPointer())))
        {
            LOG_ERROR_IF(logger, "failed to cast MultiplexingContext to TimingContextWR");
            return;
        }
        //std::cout << "WR-Event Processing - EventNumber: " << contextWR->getEventNumber() << "-  timestamp: "<< contextWR->getTimeStamp() << std::endl;

        bool event_will_trigger_digitizers = false;

        const Devices& devices = getFilteredDeviceCollection(pEvt);
        for (Devices::const_iterator it = devices.begin(); it != devices.end(); ++it)
        {
            Device* device = *it;
            boost::shared_ptr<DeviceDataBuffer> buffer = RTDeviceClass::getInstance()->getDeviceDataBuffer(device);
            if (buffer->isTriggerEvent(contextWR->getEventNumber()) &&  buffer->isTriggerEventEnabled(contextWR->getEventNumber()))
                event_will_trigger_digitizers = true;
        }

        int64_t tag_count = -1;
        if(event_will_trigger_digitizers)
        {
            tag_count = trigger_tag_count;
            trigger_tag_count ++;
            LOG_TRACE_IF(logger, "Adding trigger tag to context tracker. ID:" + std::to_string(tag_count));
        }

        std::shared_ptr<cabad::TimingContext> context = std::make_shared<CabadTimingContext>(contextWR, tag_count);
        rtDeviceClass->getContextTracker()->addContext(context);
        if(event_will_trigger_digitizers)
        {
           // std::cout << "Added trigger tag to context tracker. ID: " << tag_count << std::endl;
        }


        for (Devices::const_iterator it = devices.begin(); it != devices.end(); ++it)
        {
            Device* device = *it;
            boost::shared_ptr<DeviceDataBuffer> buffer = RTDeviceClass::getInstance()->getDeviceDataBuffer(device);
            std::string eventName =  buffer->eventID2eventName(contextWR->getEventNumber());
            device->TriggerEventName.set(eventName.c_str(),pEvt->getMultiplexingContext());
            device->acquisitionContext.insert(pEvt->getMultiplexingContext());
            registerManualNotification("SnoopTriggerEvents",device->getName());
        }
    }
    catch (const fesa::FesaException& exception)
    {
        LOG_ERROR_IF(logger, exception.getMessage());
    }
    catch (std::exception& ex)
    {
        LOG_ERROR_IF(logger, ex.what());
    }
    catch (...)
    {
        LOG_ERROR_IF(logger, "Unknown Exception in DigitizerTriggerAction::execute");
    }
    sendManualNotification(pEvt->getMultiplexingContext());

}

} // DigitizerClass2
