
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>
#include <DigitizerClass2/RealTime/UpdateDAQClients.h>
#include <DigitizerClass2/Common/Definitions.h>
#include <DigitizerClass2/RealTime/RTDeviceClass.h>
#include <DigitizerClass2/Common/Sink.h>

#include <cmw-log/Logger.h>

namespace
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.RealTime.UpdateDAQClients");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "DigitizerClass2"; \
    diagMsg.name = "UpdateDAQClients"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    DigitizerClass2ServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace DigitizerClass2
{

UpdateDAQClients::UpdateDAQClients(fesa::RTActionConfig& rtActionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses) :
     UpdateDAQClientsBase(rtActionConfig, serviceLocator, serviceLocatorRelatedClasses) 
{
}

UpdateDAQClients::~UpdateDAQClients()
{
}

// This method is called whenever an event triggers this action; this is where custom code must be written.
// A real time action operates on a device collection, this is why a loop which iterates over the device collection
// of this real time action is there.
void UpdateDAQClients::execute(fesa::RTEvent* pEvt)
{
    try
    {
        //std::cout << "UpdateTriggeredClients::execute start"<< std::endl;
        fesa::MultiplexingContext* context = pEvt->getMultiplexingContext();

        // Payload consists id of the queue element of interest
        std::string id_string(pEvt->getPayload()->getValue<char>());

        // Hack in order to define the right data-set. There is already an open Issue in FESA, in order to allow passing data directly
        context->setExtraCondition(NOTIFICATION_IDENTIFIER_PREFIX + id_string);

        for( auto device : this->DigitizerClass2ServiceLocator_->getDeviceCollection() )
        {
            registerManualNotification("AcquisitionDAQ",device->getName());
            //registerManualNotification("AcquisitionSpectra",device->getName());
        }
        sendManualNotification(pEvt->getMultiplexingContext());
        //std::cout << "UpdateTriggeredClients::execute end"<< std::endl;
    }
    catch (const fesa::FesaException& exception)
    {
        LOG_ERROR_IF(logger, exception.getMessage());
    }
    catch (std::exception& ex)
    {
        LOG_ERROR_IF(logger, ex.what());
    }
    catch (...)
    {
        LOG_ERROR_IF(logger, "Unknown Exception in UpdateTriggeredClients::execute");
    }
}

} // DigitizerClass2
