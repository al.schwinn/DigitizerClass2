/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#ifndef _DigitizerClass2_RT_DEVICE_CLASS_H_
#define _DigitizerClass2_RT_DEVICE_CLASS_H_

#include <fesa-core/Diagnostic/Defs.h>

#include <DigitizerClass2/GeneratedCode/RTDeviceClassGen.h>
#include <DigitizerClass2/GeneratedCode/TypeDefinition.h>

#include <cabad/ContextTracker.h>
#include <cabad/DataReadyManager.h>

#include <glib.h>

namespace DigitizerClass2
{
    class Device;
    class Sink;
    class DeviceDataBuffer;
    class ServerNotificationDataQueue;

class RTDeviceClass: public RTDeviceClassGen
{
public:
    static RTDeviceClass* getInstance();
    static void releaseInstance();
    static void logDiagnosticMessage(const std::string& topic,fesa::DiagnosticsDefs::DiagnosticMessage& diagMsg);

    void specificInit();
    void specificShutDown();

    cabad::ContextTracker* getContextTracker()
    {
        return contextTracker_.get();
    }

    cabad::DataReadyManager* getDAQDataReadyManager()
    {
        return daqDataReadyManager_.get();
    }

    Sink* getSink(const Device* pDev, std::string sinkID);

    boost::shared_ptr<DeviceDataBuffer> getDeviceDataBuffer(Device* pDev);

    void forAllSinks(std::function<void(Sink*)> const &func);

    void set_utc_offset(int64_t offset)
    {
        utc_offset_ = offset;

    }
    int64_t get_utc_offset()
    {
        return utc_offset_;
    }

    /*!
     * \brief Proper shotdown decreases risk of connection trouble for ps6000 scopes (For other scopes this seems not to matter much)
     */
    void closeDigitizerConnections();

    MODULE_STATUS::MODULE_STATUS mod_status_pexaria_;
    MODULE_STATUS::MODULE_STATUS mod_status_picoscopes_;
    bool flowGraphLoaded_;
    bool sampleRateValid_;

    void configurePexariaIOs (Device *device, std::string ftrnName);

    int32_t getUptimeInSeconds() const;

    // Check if currently a resync is ongoing
    bool resyncTriggerTags();

    // Request a resync
    void requestResyncTriggerTags();

private:
    RTDeviceClass();
    virtual ~RTDeviceClass();
    static RTDeviceClass* instance_;

    // Sinks( and Data Buffers) per DeviceName
    std::map<std::string, boost::shared_ptr<DeviceDataBuffer> > deviceDataCol_;


    boost::scoped_ptr<cabad::ContextTracker> contextTracker_;

    boost::scoped_ptr<cabad::DataReadyManager> daqDataReadyManager_;

    // White Rabbit UTC Time and System UTC time will differ up to 5ms in the worst case (user net)
    // In order to  we will need to add the offset to each gnuradio tag timestamp
    int64_t utc_offset_;

    std::chrono::system_clock clock_;
    std::chrono::time_point<std::chrono::system_clock> class_start_time_;

    bool resyncTriggerTags_;
    std::mutex resyncTriggerTagsMutex_;
    std::chrono::time_point<std::chrono::system_clock> resyncTriggerTagsTime_;

    /*!
     * \brief Configures an IO of a given timing device as an output and sets the
     * output line to low.
     */
    void configureOutput(const std::string &devName, const std::string &ioName);

    /*!
     * \brief Creates a new active ouput condition on a given output.
     * \param offset Offset in nanoseconds
     * \param ioEdge If set to true the line is driven high, else low
     */
    void createOutputCondition(const std::string &devName, const std::string &ioName,
            guint64 eventID, guint64 eventMask, gint64 offset, bool ioEdge);

    /*!
     * \brief Destroys all unown conditions for a given IO.
     */
    void destroyOutputConditions(const std::string &devName, const std::string &ioName);
};

} // DigitizerClass2

#endif // _DigitizerClass2_RT_DEVICE_CLASS_H_
