/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#ifndef _DigitizerClass_DeviceDataBuffer_H_
#define _DigitizerClass_DeviceDataBuffer_H_

#include <cabad/DeviceDataBufferBase.h>

#include <fesa-core/Synchronization/NoneContext.h>

#include <flowgraph/flowgraph.h>

namespace DigitizerClass2
{

class Sink;
struct SinkParams;

class DeviceDataBuffer : public cabad::DeviceDataBufferBase
{
public:

    typedef std::map<std::string, std::unique_ptr<Sink> > SinksByChannelNameMapType;

    typedef std::map <unsigned int, std::string> TriggerEventNamesByEventIDType;

    DeviceDataBuffer(std::vector<std::string>& fesaConcreteEventNames,
                     const std::string& fesaDeviceName,
                     const std::string& refTriggerEvent,
                     const std::string& refTriggerEvent_fallback1,
                     const std::string& refTriggerEvent_fallback2);

    Sink* getSinkById(const std::string& sink_name);

    Sink* getSinkByChannelName(std::string const &channel_name);

    void destroyAllSinks();

    void forAllSinks(std::function<void(Sink*)> const &func);

    void addSink(const SinkParams& params);

    void setTriggerMatchingTolerance(int64_t trigger_matching_tolerance);

    void setFlowGraph(std::shared_ptr<flowgraph::FlowGraph> flowgraph);

    std::shared_ptr<flowgraph::FlowGraph> getFlowGraph();
private:

    SinksByChannelNameMapType sinkMap_;

    fesa::NoneContext none_context_;

    std::shared_ptr<flowgraph::FlowGraph> flowgraph_;
};

} // DigitizerClass

#endif // include guard
