/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#ifndef SRC_DIGITIZERCLASS_COMMON_FLOWGRAPHLOADER_H_
#define SRC_DIGITIZERCLASS_COMMON_FLOWGRAPHLOADER_H_

#include <DigitizerClass2/Common/Utils.h>

#include <vector>
#include <memory>
#include <map>
#include <condition_variable>
#include <mutex>
#include <thread>
#include <flowgraph/flowgraph.h>

#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/smart_ptr/make_shared.hpp>
#include <boost/function.hpp>

namespace DigitizerClass2
{

typedef boost::function<void(const gr::digitizers::data_available_event_t *)> data_available_event_function;

static void invoke_function(const gr::digitizers::data_available_event_t *event, void *ptr)
{
    (*static_cast<data_available_event_function *>(ptr))(event);
}

// forward definition
class Device;

/*!
 * \brief A class aggregating callback from GNU Radio data sinks. Note some
 * tricks are used (lambda capturing & std::function) in order to bundle
 * device name and sink id to the callback.
 */
class DataReadyCallable
{
public:
    void wait(std::string &id, int64_t &timestamp)
    {
        gr::digitizers::data_available_event_t event;
        queue_.wait_and_pop(event);

        id = event.signal_name;
        timestamp = event.trigger_timestamp;
    }

    template<class TSink>
    void register_callback(const std::string &deviceName, const std::string &id, TSink *sink)
    {
        auto callback = boost::make_shared<data_available_event_function>([this, deviceName, id] (const gr::digitizers::data_available_event_t *event)
        {
            gr::digitizers::data_available_event_t tmp = *event;
            // Hack: signal name is abused to convey device name (or flowgraph name) + unique sink id
            tmp.signal_name = (deviceName + "." + id);
            queue_.push(tmp);
        });

        sink->set_callback(&invoke_function, static_cast<void *>(callback.get()));

        callbacks_.push_back(std::move(callback));
    }

    /*!
     * Make sure to stop the flowgraph before you call this method!!!
     */
    void remove_callbacks()
    {
        callbacks_.clear();
    }

private:
    ConcurrentQueue<gr::digitizers::data_available_event_t> queue_;
    std::vector<boost::shared_ptr<data_available_event_function>> callbacks_;
};

typedef boost::function<void(int64_t)> interlock_function;

static void invoke_function(int64_t timestamp, void *ptr)
{
    (*static_cast<interlock_function *>(ptr))(timestamp);
}

/*!
 * \brief Very similar to the ::DataReadyCallable.
 */
class InterlockCallable
{
public:
    struct InterlockEvent
    {
        std::string id;
        int64_t timestamp;
    };

    void wait(std::string &id, int64_t &timestamp)
    {
        InterlockEvent event;
        queue_.wait_and_pop(event);
        id = event.id;
        timestamp = event.timestamp;
    }

    void register_callback(const std::string &deviceName, const std::string &id, gr::digitizers::interlock_generation_ff *interlock)
    {
        auto callback = boost::make_shared<interlock_function>([this, deviceName, id] (int64_t timestamp)
        {
            InterlockEvent event;
            event.id = (deviceName + "." + id);
            event.timestamp = timestamp;
            queue_.push(event);
        });

        interlock->set_callback(&invoke_function, static_cast<void *>(callback.get()));

        callbacks_.push_back(std::move(callback));
    }

    /*!
     * Make sure to stop the flowgraph before you call this method!!!
     */
    void remove_callbacks()
    {
        callbacks_.clear();
    }

private:
    ConcurrentQueue<InterlockEvent> queue_;
    std::vector<boost::shared_ptr<interlock_function>> callbacks_;
};

/*!
 * \brief A helper class (singleton) holding references to all the flowgraphs.
 *
 * It is assumed that each device instance has a single flowgraph assigned to.
 * A single flowgraph can make use of one or more digitizer devices.
 */
class FlowGraphLoader
{
public:

    FlowGraphLoader(FlowGraphLoader const&) = delete;
    FlowGraphLoader(FlowGraphLoader&&) = delete;
    FlowGraphLoader& operator=(FlowGraphLoader const&) = delete;
    FlowGraphLoader& operator=(FlowGraphLoader &&) = delete;

    /*!
     * \brief Gets registry instance.
     * \returns flowgraph registry
     */
    static FlowGraphLoader& getInstance()
    {
        static FlowGraphLoader instance;
        return instance;
    }

    /*!
     * \brief Load a flowgraph for particular device instance.
     * \param device fesa Device
     */
    void loadFlowgraph(Device* device);

    /*!
     * \brief Resets/reloads previously created flowgraph.
     *
     * Note, previous flowgraph might still be used but it will eventually go out
     * of scope and it will be destructed (we use shared pointers). Also note, all the
     * callbacks registered for the previously created flowgraph are disconnected as well.
     *
     * Make sure to stop the flowgraph before calling this method!!!
     */
    void resetFlowgraph(Device* device);

    /*!
     * This is a blocking function returning only if some data is ready.
     */
    void waitDataReady(std::string &id, int64_t &timestamp);

    void waitSpectraDataReady(std::string &id, int64_t &timestamp);

    void waitInterlock(std::string &id, int64_t &timestamp);

    void startFlowGraph(Device* device);

    void stopFlowGraph(Device* device);

private:

    std::string readFlowgraphFromFile (Device* device);

    void setFlowgraphInfoToDevice (std::shared_ptr < flowgraph::FlowGraph > flowgraph, Device* device);

    void throwDigitizerErrors(std::shared_ptr < flowgraph::FlowGraph > flowgraph);

    FlowGraphLoader() = default;

    DataReadyCallable callable_;
    DataReadyCallable scallable_;
    InterlockCallable icallable_;
    mutable std::mutex mutex_;
};

}
;

#endif /* SRC_DIGITIZERCLASS_COMMON_FLOWGRAPHLOADER_H_ */
