/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#pragma once

#include <fesa-core-gsi/Synchronization/TimingContextWR.h>

namespace DigitizerClass2
{

/* Required in order to inject a WR context into the cabad library */
class CabadTimingContext: public cabad::TimingContext
{
    public:
        // returns timestamp in nanosecond precision
        int64_t getTimeStamp() const override
        {
            return contextWR_->getTimeStamp();
        }

        // We assume that different events which can occur are numbered
        unsigned int getEventNumber() const override
        {
            return contextWR_->getEventNumber();
        }

        int64_t getID() const override
        {
            return trigger_tag_count_;
        }

        CabadTimingContext(boost::shared_ptr<fesaGSI::TimingContextWR>& contextWR, int64_t trigger_tag_count)
        {
            contextWR_ = contextWR;
            trigger_tag_count_ = trigger_tag_count;
        }

        boost::shared_ptr<fesaGSI::TimingContextWR> getWRContext() const
        {
            return contextWR_;
        }

    private:

        boost::shared_ptr<fesaGSI::TimingContextWR> contextWR_;

        int64_t trigger_tag_count_;
};

} // DigitizerClass
