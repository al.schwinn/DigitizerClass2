/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#ifndef SRC_DIGITIZERCLASS_COMMON_UTILS_H_
#define SRC_DIGITIZERCLASS_COMMON_UTILS_H_

#include <DigitizerClass2/GeneratedCode/TypeDefinition.h>

#include <queue>
#include <cstddef>
#include <cassert>

#include <boost/noncopyable.hpp>
#include <boost/range/irange.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>

namespace DigitizerClass2
{
    /*!
     * \brief A trivial concurrent queue.
     */
    template <typename T>
    class ConcurrentQueue : private boost::noncopyable
    {
      private:
        std::queue<T> queue_;
        boost::condition_variable cv_;
        mutable boost::mutex mutex_;

      public:
        ConcurrentQueue(){}

        void push(T new_value)
        {
            {
                boost::mutex::scoped_lock lock(mutex_);
                queue_.push(new_value);
            }
            cv_.notify_one();
        }

        void wait_and_pop(T& value)
        {
            boost::unique_lock<boost::mutex> lock(mutex_);
            cv_.wait(lock, [this] { return !queue_.empty(); });

            value = queue_.front();
            queue_.pop();
        }

        bool empty() const
        {
            boost::mutex::scoped_lock lock(mutex_);
            return queue_.empty();
        }
    };

} // namespace

#endif /* SRC_DIGITIZERCLASS_COMMON_UTILS_H_ */
