/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <DigitizerClass2/Common/DeviceDataBuffer.h>
#include <DigitizerClass2/Common/Sink.h>

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::event); \
    diagMsg.fesaClass = "DigitizerClass2"; \
    diagMsg.name = "SharedState"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    serviceLocator->logDiagnosticMessage(topic, diagMsg); \
}

namespace
{

  cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.DeviceDataBuffer");

} // namespace

namespace DigitizerClass2
{

DeviceDataBuffer::DeviceDataBuffer(std::vector<std::string>& fesaConcreteEventNames,
                                   const std::string& fesaDeviceName,
                                   const std::string& refTriggerEvent,
                                   const std::string& refTriggerEvent_fallback1,
                                   const std::string& refTriggerEvent_fallback2) :
                  cabad::DeviceDataBufferBase(fesaConcreteEventNames,
                                              fesaDeviceName,
                                              refTriggerEvent,
                                              refTriggerEvent_fallback1,
                                              refTriggerEvent_fallback2)
{

}

Sink* DeviceDataBuffer::getSinkById(const std::string& sink_name)
{
    auto it = sinkMap_.find(sink_name);
    if (it == sinkMap_.end()) {
        throw fesa::FesaException(__FILE__, __LINE__, "Logic Error: sink not found: " + sink_name);
    }
    return it->second.get();
}

Sink* DeviceDataBuffer::getSinkByChannelName(std::string const &channel_name)
{
    auto it = sinkMap_.find(channel_name);
    if (it == sinkMap_.end())
        return nullptr;
    return (it->second).get();
}

void DeviceDataBuffer::forAllSinks(std::function<void(Sink*)> const &func)
{
    for (auto &elem : sinkMap_) {
        func(elem.second.get());
    }
}

void DeviceDataBuffer::addSink(const SinkParams& params)
{
    auto it = sinkMap_.find(params.signal_name);
    if (it != sinkMap_.end()) {
        throw fesa::FesaException(__FILE__, __LINE__, "Logic Error: sink for channel '" + params.signal_name + "' already exists.");
    }
    sinkMap_.insert( std::make_pair(params.signal_name, std::unique_ptr<Sink>(new Sink(params, this))));
}

void DeviceDataBuffer::destroyAllSinks()
{
    sinkMap_.clear();
}

void DeviceDataBuffer::setTriggerMatchingTolerance(int64_t trigger_matching_tolerance)
{
    for (auto &elem : sinkMap_)
      elem.second->getCircularBufferManager()->setTriggerMatchingTolerance(trigger_matching_tolerance);
}

void DeviceDataBuffer::setFlowGraph(std::shared_ptr<flowgraph::FlowGraph> flowgraph)
{
    flowgraph_ = flowgraph;
}

std::shared_ptr<flowgraph::FlowGraph> DeviceDataBuffer::getFlowGraph()
{
    return flowgraph_;
}

} // DigitizerClass
