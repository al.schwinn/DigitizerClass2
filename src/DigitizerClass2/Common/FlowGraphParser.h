/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#ifndef SRC_DIGITIZERCLASS_COMMON_FLOWGGRAPHPARSER_H_
#define SRC_DIGITIZERCLASS_COMMON_FLOWGGRAPHPARSER_H_

#include <libxml/parser.h>

namespace DigitizerClass2
{
    class FlowGraphParser
    {

    public:

        FlowGraphParser(std::string xml_string);
        ~FlowGraphParser();
        std::string readGrBlockParameter(std::string block_name, std::string parameter);
        void writeGrBlockParameter(std::string block_name, std::string parameter, std::string value);
        std::string document_to_string();

        // One entry per parameter, format: <Block>:<Parameter>:<Value>,<Block>:<Parameter>:<Value>,<Block>:<Parameter>:<Value>,...
        void document_to_param_value_tokens(std::vector<std::string>& tokens);

    private:

        xmlNodePtr executeXpath(std::string xpathExpr, std::function<void(xmlNodePtr)> function_to_execute);
        xmlNodePtr executeOnNode(std::string block_name, std::string parameter, std::function<void(xmlNodePtr)> function_to_execute);

        xmlDocPtr document_;
    };

} // namespace

#endif /* SRC_DIGITIZERCLASS_COMMON_FLOWGGRAPHPARSER_H_ */
