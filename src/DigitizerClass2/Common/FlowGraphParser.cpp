/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <DigitizerClass2/Common/FlowGraphParser.h>
#include <DigitizerClass2/Common/Definitions.h>

#include <fesa-core/Exception/FesaException.h>

#include <stdio.h>
#include <libxml/xpath.h>
#include <libxml/xmlerror.h>
#include <iostream>
namespace DigitizerClass2
{
    FlowGraphParser::FlowGraphParser(std::string xml_string)
    {
        /*
          * this initialize the library and check potential ABI mismatches
          * between the version it was compiled for and the actual shared
          * library used.
          */
        LIBXML_TEST_VERSION
        document_ = xmlReadDoc((xmlChar *)xml_string.c_str(),NULL,NULL,XML_PARSE_COMPACT);
    }

    FlowGraphParser::~FlowGraphParser()
    {
        xmlFreeDoc(document_);       // free document
        //xmlCleanupParser();    // Free globals (DOnt do that, since FESA as well uses this parser)
    }

    std::string FlowGraphParser ::readGrBlockParameter(std::string block_name, std::string parameter)
    {
        std::string data;
        auto callback = [&data](xmlNodePtr node)
                        {
                            data = (char*)xmlNodeGetContent(node);
                        };
        executeOnNode(block_name, parameter, callback);
        return data;
    }

    void FlowGraphParser::writeGrBlockParameter(std::string block_name, std::string parameter, std::string value)
    {
        auto callback = [value](xmlNodePtr node)
                        {
                            xmlNodeSetContent(node, BAD_CAST( value.c_str()));
                        };
        executeOnNode(block_name, parameter, callback);
    }

    std::string FlowGraphParser::document_to_string()
    {
        std::string out;
        xmlChar *s;
        int size;
        xmlDocDumpMemory(document_, &s, &size);
        if (s == NULL)
            throw std::bad_alloc();
        try {
            out = (char *)s;
        } catch (...) {
            xmlFree(s);
            throw;
        }
        xmlFree(s);
        return out;
    }

    void FlowGraphParser::document_to_param_value_tokens(std::vector<std::string>& tokens)
    {
        auto callback = [&tokens](xmlNodePtr node)
                        {
                            std::string key, value;
                            for (xmlNodePtr block = node->children; block != NULL; block = block->next )
                            {
                                if(strcmp((char*)block->name, "block") != 0)
                                    continue; //only blocks here !

                                // first fill blockname
                                std::string block_name = "";
                                for (xmlNodePtr param = block->children; param != NULL; param = param->next )
                                {
                                    if(strcmp((char*)param->name, "param") != 0)
                                        continue;//only params here!
                                    for (xmlNodePtr keyvalue = param->children; keyvalue != NULL; keyvalue = keyvalue->next )
                                    {
                                        if(strcmp((char*)keyvalue->name, "key") == 0)
                                            key = (char*)xmlNodeGetContent(keyvalue);
                                        if(strcmp((char*)keyvalue->name, "value") == 0)
                                            value = (char*)xmlNodeGetContent(keyvalue);
                                        if(!key.empty() && !value.empty())
                                        {
                                            if(key=="id")
                                                block_name = value;
                                            key = value = "";
                                            break;
                                        }
                                    }
                                }

                                //than fill parameters
                                for (xmlNodePtr param = block->children; param != NULL; param = param->next )
                                {
                                    if(strcmp((char*)param->name, "param") != 0)
                                        continue;//only params here!
                                    for (xmlNodePtr keyvalue = param->children; keyvalue != NULL; keyvalue = keyvalue->next )
                                    {
                                        if(strcmp((char*)keyvalue->name, "key") == 0)
                                            key = (char*)xmlNodeGetContent(keyvalue);
                                        if(strcmp((char*)keyvalue->name, "value") == 0)
                                            value = (char*)xmlNodeGetContent(keyvalue);
                                        if(!key.empty() && !value.empty())
                                        {
                                            std::string paramvalue = block_name + BLOCK_PARAM_VALUE_SEPARATOR + key + BLOCK_PARAM_VALUE_SEPARATOR + value;
                                            //std::cout << paramvalue << std::endl;
                                            tokens.push_back(paramvalue);
                                            key = value = "";
                                        }
                                    }
                                }
                            }
                        };
        executeXpath("/flow_graph", callback);
    }

    xmlNodePtr FlowGraphParser::executeXpath(std::string xpathExpr, std::function<void(xmlNodePtr)> function_to_execute)
    {
        /* Create xpath evaluation context */
        xmlXPathContextPtr xpathCtx = xmlXPathNewContext(document_);
        if(xpathCtx == NULL)
            throw fesa::FesaException(__FILE__, __LINE__, ": Unable to create new XPath context");

        /* Evaluate xpath expression */
        xmlXPathObjectPtr xpathObj = xmlXPathEvalExpression((xmlChar *)xpathExpr.c_str(), xpathCtx);
        if(xpathObj == NULL)
        {
            xmlXPathFreeContext(xpathCtx);
            throw fesa::FesaException(__FILE__, __LINE__, ": Unable to evaluate xpath expression");
        }
        if(xmlXPathNodeSetIsEmpty(xpathObj->nodesetval))
        {
            xmlXPathFreeObject(xpathObj);
            xmlXPathFreeContext(xpathCtx);
            throw fesa::FesaException(__FILE__, __LINE__, ": No xml node found in flowgraph");
        }
        xmlNodeSetPtr nodeset = xpathObj->nodesetval;
        if (!nodeset->nodeNr)
        {
            xmlXPathFreeObject(xpathObj);
            xmlXPathFreeContext(xpathCtx);
            throw fesa::FesaException(__FILE__, __LINE__, ": No  xml node found in flowgraph for block");
        }
        if (nodeset->nodeNr != 1)
        {
            xmlXPathFreeObject(xpathObj);
            xmlXPathFreeContext(xpathCtx);
            throw fesa::FesaException(__FILE__, __LINE__, ": Multiple xml nodes found in flowgraph");
        }

        xmlNodePtr node = nodeset->nodeTab[0];
        function_to_execute(node);

        /* Cleanup */
        xmlXPathFreeObject(xpathObj);
        xmlXPathFreeContext(xpathCtx);
    }

    xmlNodePtr FlowGraphParser::executeOnNode(std::string block_name, std::string parameter, std::function<void(xmlNodePtr)> function_to_execute)
    {
        try
        {
            std::string xpathExpr = "/flow_graph/block[param/value='" + block_name + "']/param[key='" + parameter + "']/value";
            std::cout << "xpathExpr: " << xpathExpr <<  std::endl;
            executeXpath(xpathExpr, function_to_execute);
        }
        catch(std::exception ex)
        {
            throw fesa::FesaException(__FILE__, __LINE__, ": Failed to execute XPath for block" + BLOCK_PARAM_VALUE_SEPARATOR + "param: '" + block_name + BLOCK_PARAM_VALUE_SEPARATOR + parameter + "' Error: " + ex.what());
        }
        catch(...)
        {
            throw fesa::FesaException(__FILE__, __LINE__, ": Unknown error while executing XPath for block" + BLOCK_PARAM_VALUE_SEPARATOR + "param: '" + block_name + BLOCK_PARAM_VALUE_SEPARATOR + parameter + "'" );
        }
    }

} // namespace

