/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <DigitizerClass2/Common/Sink.h>
#include <DigitizerClass2/Common/DeviceDataBuffer.h>

#include <cstdint>
#include <cassert>
#include <stdexcept>
#include <mutex>
#include <utility>
#include <string>

#include <DigitizerClass2/Common/Utils.h>
#include <DigitizerClass2/RealTime/RTDeviceClass.h>
#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>

#include <cabad/ContextTracker.h>
#include <cabad/CircularBufferBase.h>

#include <fesa-core/Synchronization/NoneContext.h>
#include <fesa-core-gsi/Synchronization/TimingContextWR.h>

#include <digitizers/status.h>

// FIXME: Remove Magic number !!
#define LOCAL_DATA_READ_ITERATORS 50

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::event); \
    diagMsg.fesaClass = "DigitizerClass2"; \
    diagMsg.name = "SharedState"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    serviceLocator->logDiagnosticMessage(topic, diagMsg); \
}

namespace
{

  cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.Sink");

} // namespace

namespace DigitizerClass2
{

// TODO: Possible values should be gathered from the FESA class
std::vector<cabad::MaxClientUpdateFrequencyType> maxClientUpdateFrequencies = {1,10,25};

void Sink::cb_copy_data(const float            *values,
                        std::size_t             values_size,
                        const float            *errors,
                        std::size_t             errors_size,
                        std::vector<gr::tag_t>& tags,
                        void*                   userdata)
{
    //std::cout << "CircularBufferManager::cb_copy_data"  << std::endl;
    if(userdata == nullptr)
        throw RUNTIME_ERROR("Logical Error: Cannot call method on object, no object passed in userdata");
    CircularBufferManager *bufferManager = static_cast<CircularBufferManager*>(userdata);
    bufferManager->push(values, values_size, errors, errors_size, tags);
}

char const * sinkTypeToStr(SinkType sink_type)
{
    switch (sink_type) {
        case SinkType::Time:
            return "Time";
        case SinkType::Frequency:
            return "Frequency";
        case SinkType::PostMortem:
            return "PostMortem";
        default:
            return "???";
    }
}

char const * sinkModeToStr(SinkMode sink_mode)
{
    switch (sink_mode) {
        case SinkMode::Streaming:
            return "Streaming";
        case SinkMode::Triggered:
            return "Triggered";
        default:
            return "???";
    }
}

Sink::Sink(const SinkParams &params, DeviceDataBuffer* deviceDataBuffer)
:
    m_gr_sink(params.sink),
    m_sink_name(params.sink_name),
    m_sink_type(SinkType::Time),  // TODO: Possibly we need a sub-type of "Sink" per sink-type? Or just a separate argument ?
    m_sink_mode(params.sink_mode),
    m_signal_name(params.sink->get_metadata().name),
    m_unit(params.sink->get_metadata().unit),
    m_sample_rate(params.sink->get_sample_rate()),
    m_output_buffer_size(params.sink->get_output_package_size())
{
    try
    {
        if (m_sink_mode != SinkMode::Streaming && m_sink_mode != SinkMode::Triggered)
        {
            throw fesa::FesaException(__FILE__, __LINE__, "Logic Error: bad sink_mode: " + std::to_string(int(m_sink_mode)));
        }

        RTDeviceClass* deviceClass = RTDeviceClass::getInstance();
        std::size_t meta_buffer_max_size = deviceClass->getContextTracker()->max_size();
        cabad::MetaDataBuffer *metadataBuffer = new cabad::MetaDataBuffer(meta_buffer_max_size, deviceDataBuffer);

        char hostname[HOST_NAME_MAX];
        gethostname(hostname, HOST_NAME_MAX);

        // Currently cabad cannot properly synchronize data-streams across multiple digitizers
        // So for now the solution is, to use the Device-Nomenclature to group channels
        std::string notification_group;
        std::size_t separator =  m_signal_name.find(":");
        if(separator == std::string::npos)
        {
            notification_group = m_signal_name;
        }
        else
        {
            std::string nomen = std::string(m_signal_name.begin(), m_signal_name.begin() + separator);
            notification_group = nomen;
            //std::cout << "Notification group: " << notification_group << std::endl;
        }


        if (strcmp (hostname,"dal005") == 0)
        {
            std::cout << "Attention !!!!! Exception: For dal005, we need to force a separate NotificationHandlers per channel" << std::endl;
            notification_group = m_signal_name;
        }

        else if (strcmp (hostname,"dal007") == 0)
        {

            // Currently cabad cannot properly synchronize data-streams across multiple digitizers
            // So for now the solution is, to use the Device-Nomenclature to group channels
            // (Exception is dal007, where we use a hack for now, until cabad is able to sync for multiple digitizers)
            // FIXME: Extra-group forfirst scope of dal007 - Remove this dirty HAck!!
            if(m_signal_name.find("GS11MU2:Current_Set")            != std::string::npos ||
               m_signal_name.find("GS11MU2:Voltage_Set")            != std::string::npos ||
               m_signal_name.find("GS11MU2:DeltaCurrent_Circuit")   != std::string::npos ||
               m_signal_name.find("GS11MU2:Current_Circuit")        != std::string::npos ||
               m_signal_name.find("GS11MU2:Voltage_1")              != std::string::npos ||
               m_signal_name.find("GS11MU2:Voltage_2")              != std::string::npos ||
               m_signal_name.find("GS11MU2:Voltage_3")              != std::string::npos ||
               m_signal_name.find("GSCD002:TIMING_1")               != std::string::npos )
            {
                notification_group += "_extra_group";
            }
        }

        // These are prefixed with Fesa DeviceName, so would land in the same notification group on multi-scope setup
        if(m_signal_name.find("TIMING") != std::string::npos)
            notification_group = m_signal_name;
        if(m_signal_name.find("SPARE") != std::string::npos)
            notification_group = m_signal_name;

        if(m_sink_mode == SinkMode::Streaming)
        {
            std::size_t size_buffer = std::size_t( ceil( m_sample_rate * float(params.bufferTime)));
            std::cout << "Creating circular buffer for streaming Sink: " << m_signal_name << " of size " << size_buffer << " elements (" << params.bufferTime<< "seconds)" << " - notification group: " << notification_group << std::endl;
            CIRCULAR_BUFFER_TYPE *circularBuffer = new CIRCULAR_BUFFER_TYPE(size_buffer);
            sanityCheckOutputBufferSize();

            // FIXME: Temporary only FULL_SEQUENCE for channels > 25H
            cabad::ClientNotificationType clientNotifType;
            if(m_sample_rate > 25. )
                clientNotifType = cabad::ClientNotificationType::STREAMING | cabad::ClientNotificationType::FULL_SEQUENCE;
            else
                clientNotifType = cabad::ClientNotificationType::STREAMING;

            m_circular_buffer_manager.reset(new CircularBufferManager(m_signal_name,
                                                                       m_sample_rate,
                                                                       maxClientUpdateFrequencies,
                                                                       deviceClass->getContextTracker(),
                                                                       deviceDataBuffer,
                                                                       circularBuffer,
                                                                       metadataBuffer,
                                                                       clientNotifType,
                                                                       cabad::DataPushMode::CONTINOUS_MULTI_SAMPLES,
                                                                       deviceClass->getDAQDataReadyManager(),
                                                                       notification_group));
        }
        else
        {
            std::size_t numberOfTriggers = deviceDataBuffer->getTriggerEvents().size();
            std::size_t pre_samples = params.sink->get_pre_samples();
            std::size_t post_samples = params.sink->get_post_samples();

            std::vector<cabad::MaxClientUpdateFrequencyType> maxClientUpdateFrequencies = {};

            // We keep the data of a maximum of two trigger-events per trigger-event-id
            std::size_t size_buffer = (pre_samples + post_samples) * numberOfTriggers * 2;
            std::cout << "Creating circular buffer for triggered Sink: " << m_signal_name << " of size " << size_buffer << " elements" << " - notification group: " << notification_group << std::endl;
            CIRCULAR_BUFFER_TYPE *circularBuffer = new CIRCULAR_BUFFER_TYPE(size_buffer);
            m_circular_buffer_manager.reset(new CircularBufferManager(m_signal_name,
                                                                   m_sample_rate,
                                                                   maxClientUpdateFrequencies,
                                                                   deviceClass->getContextTracker(),
                                                                   deviceDataBuffer,
                                                                   circularBuffer,
                                                                   metadataBuffer,
                                                                   cabad::ClientNotificationType::TRIGGERED,
                                                                   cabad::DataPushMode::DISJUNCT_MULTI_SAMPLES,
                                                                   deviceClass->getDAQDataReadyManager(),
                                                                   notification_group));
            m_circular_buffer_manager->setTriggerSamples(pre_samples, post_samples);

        }
        m_gr_sink->set_callback(Sink::cb_copy_data, m_circular_buffer_manager.get());
       // LOG_TRACE_IF(logger, "callback registered for sink: " + id);
    }
    catch(fesa::FesaException &ex)
    {
        throw;
    }
    catch(std::exception &ex)
    {
        throw fesa::FesaException(__FILE__, __LINE__, "Failed to build Sink: " + std::string(ex.what()));
    }
}

void Sink::sanityCheckOutputBufferSize() const
{
    int max_freq = 0;
    for (auto freq :maxClientUpdateFrequencies)
    {
        if(m_sample_rate > freq)
        {
            size_t max_output_buffer_size =  m_sample_rate / freq;
            if(m_output_buffer_size > max_output_buffer_size )
                throw RUNTIME_ERROR("Sink Misconfiguration. Channel: '" + m_signal_name + "'. With the current configuration some client update frequencies cannot be served. Maximum m_output_buffer_size: " + std::to_string(max_output_buffer_size));
        }
    }
}

} // DigitizerClass
