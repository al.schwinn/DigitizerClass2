/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#ifndef _DigitizerClass_Sink_H_
#define _DigitizerClass_Sink_H_

#include <cstddef>
#include <cstdint>
#include <cassert>
#include <string>
#include <unordered_map>
#include <memory>
#include <vector>
#include <functional>

#include <boost/optional.hpp>
#include <boost/noncopyable.hpp>
#include <boost/circular_buffer.hpp>

#include <fesa-core/Synchronization/MultiplexingContext.h>
#include <fesa-core/Synchronization/NoneContext.h>

#include <digitizers/time_domain_sink.h>
#include <DigitizerClass2/Common/CircularBufferManager.h>
#include <DigitizerClass2/GeneratedCode/Device.h>

namespace DigitizerClass2
{

class DeviceDataBuffer;
class ServiceLocator;

//class CircularBufferManager; why does forward declare not work ?

enum class SinkType {Time, Frequency, PostMortem};

enum class SinkMode {Streaming, Triggered};

char const * sinkTypeToStr(SinkType sink_type);

char const * sinkModeToStr(SinkMode sink_mode);

struct SinkParams {
    std::string sink_name;
    SinkType sink_type;
    SinkMode sink_mode;
    std::string signal_name;
    std::string unit;
    gr::digitizers::time_domain_sink *sink;
    int32_t bufferTime;
};

class Sink :
    private boost::noncopyable
{
public:

    /*
     * Static callback to be used by gnuradio sinks, will cast userdata to CircularBufferManager object and call pushBackData
     * */
    static void cb_copy_data(const float            *values,
                                                  std::size_t             values_size,
                                                  const float            *errors,
                                                  std::size_t             errors_size,
                                                  std::vector<gr::tag_t>& tags,
                                                  void*                   userdata);

    Sink(const SinkParams &params, DeviceDataBuffer* deviceDataBuffer);

    inline std::string const & getSinkName() const {
        return m_sink_name;
    }
    
    inline SinkType getSinkType() const {
        return m_sink_type;
    }

    inline SinkMode getSinkMode() const {
        return m_sink_mode;
    }

    inline std::string const & getSignalName() const {
        return m_signal_name;
    }

    inline std::string const & getUnit() const {
        return m_unit;
    }

    inline float const & getSampleRate() const {
        return m_sample_rate;
    }

    inline size_t getOutputBufferSize() const {
        return m_output_buffer_size;
    }

    inline CircularBufferManager* getCircularBufferManager()
    {
        return m_circular_buffer_manager.get();
    }

private:

    // Check if OutputBufferSize is small enough to e.g. provide 25Hz update rate on a 100Hz Sink
    void sanityCheckOutputBufferSize() const;

    gr::digitizers::time_domain_sink *m_gr_sink;

    std::string const m_sink_name;
    SinkType const m_sink_type;
    SinkMode const m_sink_mode;
    std::string const m_signal_name;
    std::string const m_unit;

    size_t m_output_buffer_size;

    // The frequency in which this sink will sample data in Hz
    float m_sample_rate;

    // circular data buffer
    std::unique_ptr<CircularBufferManager> m_circular_buffer_manager;
};

} // DigitizerClass

#endif // _DigitizerClass_SharedState_H_
