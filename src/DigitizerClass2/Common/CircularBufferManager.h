/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#ifndef SRC_DIGITIZERCLASS2_COMMON_CIRCULAR_BUFFER_MANAGER_H_
#define SRC_DIGITIZERCLASS2_COMMON_CIRCULAR_BUFFER_MANAGER_H_

#include <fesa-core/Synchronization/MultiplexingContext.h>

#include <fesa-core-gsi/Synchronization/TimingContextWR.h>

#include <iterator>
#include <digitizers/tags.h>

#include <DigitizerClass2/GeneratedCode/TypeDefinition.h>
#include <DigitizerClass2/Common/Definitions.h>
#include <DigitizerClass2/RealTime/UpdateDAQClientsSrc.h>

#include <cabad/ContextTracker.h>
#include <cabad/SampleMetaData.h>
#include <cabad/CircularWindowIterator.h>
#include <cabad/CircularBufferManagerBase.h>
#include <cabad/Definitions.h>
#include <cabad/Util.h>

#include <boost/shared_ptr.hpp>

namespace DigitizerClass2
{

class Device;

/* Manages all Iterators and Meta-Data of a single CircularSampleBuffer */ 
class CircularBufferManager: public cabad::CircularBufferManagerBase
{

public:

    CircularBufferManager(std::string signal_name,
                           float sample_rate_hz,
                           std::vector<cabad::MaxClientUpdateFrequencyType> maxClientUpdateFrequencies,
                           cabad::ContextTracker* contextTracker,
                           cabad::DeviceDataBufferBase* deviceDataBuffer,
                           cabad::CircularBufferBase* circularBuffer,
                           cabad::MetaDataBuffer*   metadataBuffer,
                           cabad::ClientNotificationType clientNotificationType,
                           cabad::DataPushMode dataPushMode,
                           cabad::DataReadyManager* dataReadyManager,
                           const std::string& notification_group);

    ~CircularBufferManager();

    void push(const float                  *values,
              std::size_t                   values_size,
              const float                  *errors,
              std::size_t                   errors_size,
              const std::vector<gr::tag_t>& tags);

    bool push_tags(const std::vector<gr::tag_t>& tags);

    void push_trigger_tags_not_applied_yet();

    void copyDataWindow(cabad::CircularWindowIterator* window_iter, float* values, float* errors, const std::size_t& buffer_size, std::size_t& n_data_written);

    // Periodically called in order to controll that gnuradio provides the expected sample rate
    // E.g. when the CPU is busy, it may happen that the samp_rate on the gnuradio side drops
    // return a message on error, giving the name of the channel and the measured samp rate;
    std::string checkSampRate(float expected_sample_rate_hz, uint64_t timestamp_now_ns);

    bool lastSampRateCheckOk()
    {
        return last_samp_rate_check_ok_;
    }

private:
    // Pointer to FESA event source
    UpdateDAQClientsSrc* updateDAQClientsSrc_;

    // Last time the sample rate was checked
    uint64_t last_samp_rate_check_time_;

    // Total number of samples which were pushed till last_samp_rate_check_time_
    uint64_t last_samp_rate_check_samp_count_;

    uint64_t n_data_pushed_;

    // Total number of trigger tags received by this buffer manager
    int64_t n_trigger_tags_;

    bool last_samp_rate_check_ok_;

    cabad::average_filter<double> last_measured_samp_rates_hz_;

    std::vector<std::pair<int64_t, gr::tag_t> > trigger_tags_not_applied_yet_by_id_;

    std::shared_ptr<const cabad::CircularBufferBase::iterator> multiplexing_data_added_till_here_;
    std::shared_ptr<cabad::SampleMetadata> last_metadata_added_;
};

} /* namespace DigitizerClass2 */

#endif /* SRC_DIGITIZERCLASS2_COMMON_CIRCULAR_BUFFER_MANAGER_STREAMING_H_ */
