/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <DigitizerClass2/Common/FlowGraphLoader.h>

#include <fstream>
#include <memory>

#include <cmw-log/Logger.h>

#include <DigitizerClass2/GeneratedCode/Device.h>
#include <DigitizerClass2/RealTime/RTDeviceClass.h>
#include <DigitizerClass2/Common/DeviceDataBuffer.h>
#include <DigitizerClass2/Common/Sink.h>

#include <fesa-core/Synchronization/NoneContext.h>

#include <limits.h>
#include <unistd.h>     //readlink

namespace
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.RealTime.UserCode.FlowGraphLoader");

}

namespace DigitizerClass2
{

SinkMode getTimeSinkMode(gr::digitizers::time_domain_sink &sink)
{
    switch (sink.get_sink_mode()) {
        case gr::digitizers::time_sink_mode_t::TIME_SINK_MODE_STREAMING:
            return SinkMode::Streaming;
        case gr::digitizers::time_sink_mode_t::TIME_SINK_MODE_TRIGGERED:
            return SinkMode::Triggered;
        default:
            throw fesa::FesaException(__FILE__, __LINE__, "Cannot map time sink mode to SinkMode:" + std::to_string(int(sink.get_sink_mode())));
    }
}

// Returns the path of the folder where the executable is located
std::string executable_path()
{
    char result[PATH_MAX];
    ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
    std::string full_path(result, (count > 0) ? count : 0);
    std::size_t found = full_path.find_last_of("/\\");
    return full_path.substr(0,found);
}

std::string FlowGraphLoader::readFlowgraphFromFile (Device* device)
{
    NoneContext context;
    std::string path = device->defaultGrcPath.getAsString();

    std::ifstream file1(path);
    if (!file1) // Does not exists ? --> Possibly a relative path --> Try to prefix with binary path
        path = executable_path() + "/" + path;
    file1.close();

    // LOG_ERROR_IF(logger, "grc_path: " + path);
    std::ifstream file2(path);
    if (!file2)
        throw fesa::FesaException(__FILE__, __LINE__, "Can't open GRC file:'" + path + "'");
    std::stringstream buffer;
    buffer << file2.rdbuf();
    file2.close();

    LOG_INFO_IF(logger, "Loading flowgraph for " + device->getName() + ", from GRC file: " + path);

    if (buffer.str().size() > MAX_FLOWGRAPH_XML_LENGTH)
        throw fesa::FesaException(__FILE__, __LINE__, "Cant save xml file '" + path + "' in FESA. File exceeds MAX_FLOWGRAPH_XML_LENGTH.");
    return buffer.str();
}

void FlowGraphLoader::setFlowgraphInfoToDevice (std::shared_ptr < flowgraph::FlowGraph > flowgraph, Device* device)
{
    // Obtain driver versions, define device IDs
    std::vector<std::string> digitizerIds;
    std::vector<std::string> driverVersion;
    std::vector<std::string> hwVersion;

    flowgraph->digitizers_apply([&digitizerIds, &driverVersion, &hwVersion] (const std::string &id, gr::digitizers::digitizer_block *digitizer)
    {
        digitizerIds.push_back(id);
        driverVersion.push_back(digitizer->get_driver_version());
        hwVersion.push_back("NA"); // Available only after start/init
    });

//            int32_t digitizerCount = static_cast<int32_t>(digitizerIds.size());
//TODO: Provide versions
//            device->driverVersion_dimensions.set(&digitizerCount, 1, &context);
//            device->driverVersion_dim1_labels.set(reinterpret_cast<const char**>(&digitizerIds[0]), digitizerCount, &context);
//            device->driverVersion.set(reinterpret_cast<const char**>(&driverVersion[0]), digitizerCount, &context);
//            device->hardwareVersion.set(reinterpret_cast<const char**>(&hwVersion[0]), digitizerCount, &context);
//
//            device->gnuRadioVersion.set(gr::version().c_str(), &context);
//            device->digitizersModuleVersion.set(digitizers::version().c_str(), &context);
//            device->flowgraphModuleVersion.set(flowgraph::version().c_str(), &context);
}

void FlowGraphLoader::loadFlowgraph(Device* device)
{
    NoneContext context;

    try
    {
        boost::shared_ptr<DeviceDataBuffer> deviceDataBuffer = RTDeviceClass::getInstance()->getDeviceDataBuffer(device);
        if(deviceDataBuffer->getFlowGraph())
            throw fesa::FesaException(__FILE__, __LINE__, "Flowgraph already loaded for " + device->getName());

       // std::cout << "Stored flowgraph: " << device->flowGraph.get(&context) << std::endl;

        //std::cout << "Stored flowgraph Size: " << device->flowGraph.getAsString(&context).empty() << std::endl;
        // If no flowgraph is stored internally, load the flowgraph from the grc file
        std::string flowGraph_string = device->flowGraph.getAsString(&context);
        bool default_flowgraph_used = false;
        if(flowGraph_string.empty())
        {
          flowGraph_string = readFlowgraphFromFile(device);
          default_flowgraph_used = true;
          std::cout << "default flowGraph loaded from file"  << std::endl;
        }
        else
          std::cout << "persistent flowGraph loaded from /opt/fesa/data/ (/common/fesadata/data/$(FEC) on cluster)"  << std::endl;

        device->defaultFlowGraphUsed.set(default_flowgraph_used, &context);
        std::istringstream flowGraph_stream(flowGraph_string);

        std::shared_ptr < flowgraph::FlowGraph > flowgraph = flowgraph::make_flowgraph(flowGraph_stream);
        setFlowgraphInfoToDevice(flowgraph, device);
        deviceDataBuffer->setFlowGraph(flowgraph);
        deviceDataBuffer->destroyAllSinks();

        flowgraph->time_domain_sinks_apply(
            [&](const std::string &sink_name, gr::digitizers::time_domain_sink *sink)
        {
            gr::digitizers::signal_metadata_t metadata = sink->get_metadata();

            SinkParams sink_params;
            sink_params.sink = sink;
            sink_params.sink_name = sink_name;
            sink_params.sink_type = SinkType::Time;
            sink_params.sink_mode = getTimeSinkMode(*sink);
            sink_params.signal_name = metadata.name;
            sink_params.unit = metadata.unit;
            sink_params.bufferTime = device->bufferTime.get();
//            if (sink_params.signal_name.find("25Hz") != std::string::npos)
//                return;
//            if (sink_params.signal_name.find("Triggered") != std::string::npos)
//                return;
//            if (sink_params.signal_name.find("10Hz") == std::string::npos)
//                return;
//            if (sink_params.signal_name.find("ChannelE") == std::string::npos && sink_params.signal_name.find("ChannelD") == std::string::npos)
//                return;
//            if (sink_params.signal_name.find("100Hz") != std::string::npos)
//                return;
//            if(sink_params.signal_name == "GSI123XY4:Voltage_Rectangle@25Hz") //||
//               sink_params.signal_name == "PULSEDPOWER:Voltage_2@10kHz" ||
//               sink_params.signal_name == "PULSEDPOWER:Voltage_3@10kHz") // For debugging it is nice to only check specific channels
            deviceDataBuffer->addSink(sink_params);
        });

    //    flowgraph->freq_sinks_apply([this, &deviceName] (const std::string &id, gr::digitizers::freq_sink_f *sink)
    //    {
    //       // scallable_.register_callback(deviceName, id, sink);
    //        //LOG_TRACE_IF(logger, "spectra callback registered for: " + deviceName + ", sink: " + id);
    //    });
    //
    //    flowgraph->interlock_apply([this, &deviceName] (const std::string &id, gr::digitizers::interlock_generation_ff *sink)
    //    {
    //    //    icallable_.register_callback(deviceName, id, sink);
    //      //  LOG_TRACE_IF(logger, "interlock callback registered for: " + deviceName + ", block: " + id);
    //    });

        device->flowGraph.set(flowGraph_string,&context);
    }
    catch(std::exception& ex)
    {
        throw fesa::FesaException(__FILE__, __LINE__, "Error loading flowgraph: " + std::string(ex.what()));
    }
    catch(...)
    {
        throw fesa::FesaException(__FILE__, __LINE__, "Unknown Error loading flowgraph" );
    }
}

void FlowGraphLoader::throwDigitizerErrors(std::shared_ptr < flowgraph::FlowGraph > flowgraph)
{
    flowgraph->digitizers_apply(
        [](const std::string &id, gr::digitizers::digitizer_block *digitizer)
    {
        if(!digitizer->getConfigureExceptionMessage().empty())
        {
            throw fesa::FesaException(__FILE__, __LINE__, digitizer->getConfigureExceptionMessage());
        }
    });
}

void FlowGraphLoader::startFlowGraph(Device* device)
{
    boost::shared_ptr<DeviceDataBuffer> deviceDataBuffer = RTDeviceClass::getInstance()->getDeviceDataBuffer(device);
    auto flowgraph = deviceDataBuffer->getFlowGraph();

    try
    {
        auto flowgraph = deviceDataBuffer->getFlowGraph();
        if(!flowgraph)
            throw fesa::FesaException(__FILE__, __LINE__, "No flowgraph defined for device: " + device->getName());
        std::cout << "-- Staring flowgraph --" << std::endl;
        flowgraph->start();

        flowgraph->digitizers_apply([] (const std::string &id, gr::digitizers::digitizer_block *digitizer)
        {
            if(digitizer->get_acquisition_mode() == gr::digitizers::STREAMING)
            {
                std::cout << "-- Arming Digitizer " << id <<  "--" << std::endl;
                digitizer->arm();
            }
        });
    }
    catch(std::string& ex)
    {
        throw fesa::FesaException(__FILE__, __LINE__, "Reason: Logical Error in grc file: " + ex);
    }
    catch(std::exception& ex)
    {
        throw fesa::FesaException(__FILE__, __LINE__, "Reason: Logical Error in grc file: " + std::string(ex.what()));
    }
    catch(...)
    {
        throwDigitizerErrors (flowgraph);
        throw fesa::FesaException(__FILE__, __LINE__, "Reason: Unknown Error in grc file: " );
    }
}

void FlowGraphLoader::stopFlowGraph(Device* device)
{
    boost::shared_ptr<DeviceDataBuffer> deviceDataBuffer = RTDeviceClass::getInstance()->getDeviceDataBuffer(device);
    auto flowgraph = deviceDataBuffer->getFlowGraph();

    try
    {
        if(!flowgraph)
            throw fesa::FesaException(__FILE__, __LINE__, "No flowgraph defined for device: " + device->getName());
        flowgraph->stop();
        //buffer->destroyAllSinks();

    }
    catch(std::exception& ex)
    {
        throw fesa::FesaException(__FILE__, __LINE__, "Reason: Logical Error in grc file: " + std::string(ex.what()));
    }
    catch(...)
    {
        throwDigitizerErrors (flowgraph);
        throw fesa::FesaException(__FILE__, __LINE__, "Reason: Unknown Error in grc file: " );
    }
}

void FlowGraphLoader::resetFlowgraph(Device* device)
{
    // TODO: Reload flowgraph from grc file here ?
    {
        std::lock_guard < std::mutex > guard(mutex_);
        callable_.remove_callbacks();
        scallable_.remove_callbacks();
        icallable_.remove_callbacks();
    }
    loadFlowgraph(device);
}

void FlowGraphLoader::waitDataReady(std::string &id, int64_t &timestamp)
{
    callable_.wait(id, timestamp);
}

void FlowGraphLoader::waitSpectraDataReady(std::string &id, int64_t &timestamp)
{
    scallable_.wait(id, timestamp);
}

void FlowGraphLoader::waitInterlock(std::string &id, int64_t &timestamp)
{
    icallable_.wait(id, timestamp);
}

}
