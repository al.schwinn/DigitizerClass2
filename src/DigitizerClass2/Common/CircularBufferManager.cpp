/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include "CircularBufferManager.h"

#include <DigitizerClass2/GeneratedCode/Device.h>
#include <DigitizerClass2/Common/Sink.h>
#include <DigitizerClass2/Common/Definitions.h>
#include <DigitizerClass2/RealTime/RTDeviceClass.h>

#include <fesa-core/Synchronization/MultiplexingContext.h>
#include <fesa-core-gsi/Synchronization/Selector.h>
#include <fesa-core/Exception/FesaException.h>

#include <digitizers/status.h>

#include <cmw-log/Logger.h>

#include <vector>

#define DEBUGGING FALSE

namespace
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.CircularBufferManager");

} // namespace

namespace DigitizerClass2
{

CircularBufferManager::CircularBufferManager(std::string signal_name,
                                                               float sample_rate_hz,
                                                               std::vector<cabad::MaxClientUpdateFrequencyType> maxClientUpdateFrequencies,
                                                               cabad::ContextTracker* contextTracker,
                                                               cabad::DeviceDataBufferBase* deviceDataBuffer,
                                                               cabad::CircularBufferBase* circularBuffer,
                                                               cabad::MetaDataBuffer* metadataBuffer,
                                                               cabad::ClientNotificationType clientNotificationType,
                                                               cabad::DataPushMode dataPushMode,
                                                               cabad::DataReadyManager* dataReadyManager,
                                                               const std::string& notification_group):
              cabad::CircularBufferManagerBase(            signal_name,
                                                          sample_rate_hz,
                                                          maxClientUpdateFrequencies,
                                                          contextTracker,
                                                          deviceDataBuffer,
                                                          circularBuffer,
                                                          metadataBuffer,
                                                          clientNotificationType,
                                                          dataPushMode,
                                                          dataReadyManager,
                                                          notification_group),
                                                          updateDAQClientsSrc_(NULL),
                                                          last_measured_samp_rates_hz_(5) // always average on the last 5 measurements
{
    last_samp_rate_check_time_ = 0;
    last_samp_rate_check_samp_count_ = 0;
    n_data_pushed_ = 0;
    last_samp_rate_check_ok_ = true;
    n_trigger_tags_ = 0;
}

CircularBufferManager::~CircularBufferManager()
{
}

bool CircularBufferManager::push_tags(const std::vector<gr::tag_t>& tags)
{
    // status:
    // First 16Bits from: gr-digitizers/include/digitizers/status.h
    // Last  16Bits     : Fesa Internal
    uint32_t status = 0;

    bool all_tags_added_to_buffer = true;
    for (const auto &tag : tags)
    {
        if(tag.key == pmt::string_to_symbol(gr::digitizers::trigger_tag_name))
        {
            gr::digitizers::trigger_t trigger_tag_data = gr::digitizers::decode_trigger_tag(tag);

            status |= trigger_tag_data.status;

            LOG_DEBUG_IF(logger, getSignalName() + "- new stamp received: tag.trigger_timestamp:" + std::to_string(trigger_tag_data.timestamp) );

            //if( signal_name_ == "GE01QS9D:Current_1@1kHz")
            //    std::cout << "Received trigger tag -n_trigger_tags_: " << n_trigger_tags_ << std::endl;
            std::shared_ptr<const cabad::TimingContext> context = contextTracker_->getContextByID(n_trigger_tags_);
            if(context)
            {
                //if( signal_name_ == "GE01QS9D:Current_1@1kHz")
                //    std::cout << "Adding metadata for tag: " << std::endl;
                std::shared_ptr<cabad::SampleMetadata> new_meta = addTriggerMetaData(context, tag.offset, context->getTimeStamp());
                n_matched_triggers_.push(1.);
                if(new_meta)
                  last_metadata_added_ = new_meta;
            }
            else
            {
                trigger_tags_not_applied_yet_by_id_.push_back (std::make_pair(n_trigger_tags_, tag));
                all_tags_added_to_buffer = false;
                continue;
            }
            n_trigger_tags_++;
        }
        else if(tag.key == pmt::string_to_symbol( gr::digitizers::acq_info_tag_name))
        {
            gr::digitizers::acq_info_t acq_info_tag_data = gr::digitizers::decode_acq_info_tag(tag);
            status |= acq_info_tag_data.status;

            if(status & gr::digitizers::channel_status_t::CHANNEL_STATUS_DATA_BUFFERS_LOST)
            {
                LOG_ERROR_IF(logger, "Detected buffer loss for channel: " + getSignalName() + " This should not happen. Try to reduce the sampling rate of the scope, or increase the buffers." );
                LOG_ERROR_IF(logger, "The FESA class will now rest the trigger tag / WR-event synchronization" );

                RTDeviceClass::getInstance()->requestResyncTriggerTags();
                RTDeviceClass::getInstance()->getContextTracker()->clear();
            }

//                Dont add data with invalid timestamp
//                if( RTDeviceClass::getInstance()->get_utc_offset() == 0)
//                {
//                    return;
//                }

            //metaWriteIter_->user_delay = tag.user_delay;
            //metaWriteIter_->actual_delay = tag.actual_delay;
            // TODO: Extarct UTC stamp --> Relevant if no Timing is played
            //addWrStampMetaData(trigger_tag_data.timestamp + RTDeviceClass::getInstance()->get_utc_offset(), tag.offset, status);
            // TODO: What else needs to be extracted from acq_info ?
        }
        else
        {
            // What to do with other tags ??
        }
    }

    return all_tags_added_to_buffer;
}

void CircularBufferManager::push_trigger_tags_not_applied_yet()
{
    uint32_t status = 0;

    for (auto tag = trigger_tags_not_applied_yet_by_id_.begin(); tag != trigger_tags_not_applied_yet_by_id_.end(); tag)
    {
        if(tag->second.key == pmt::string_to_symbol(gr::digitizers::trigger_tag_name))
        {
            gr::digitizers::trigger_t trigger_tag_data = gr::digitizers::decode_trigger_tag(tag->second);

            status |= trigger_tag_data.status;

            LOG_DEBUG_IF(logger, getSignalName() + "- new stamp received: tag.trigger_timestamp:" + std::to_string(trigger_tag_data.timestamp) );

            std::shared_ptr<const cabad::TimingContext> context = contextTracker_->getContextByID(tag->first);
            if(context)
            {
                //if( signal_name_ == "GE01QS9D:Current_1@1kHz")
                //    std::cout << "Adding late metadata for tag: " << std::endl;
                std::shared_ptr<cabad::SampleMetadata> new_meta = addTriggerMetaData(context, tag->second.offset, context->getTimeStamp());
                if(new_meta)
                  last_metadata_added_ = new_meta;

                n_matched_triggers_.push(1.);

                //drop matching ones from the list
                tag = trigger_tags_not_applied_yet_by_id_.erase(tag);
                continue;
            }
        }
        tag++;
    }
}


void CircularBufferManager::push(const float            *values,
                                 std::size_t             values_size,
                                 const float            *errors,
                                 std::size_t             errors_size,
                                 const std::vector<gr::tag_t>& tags)
{
    try
    {
        //    std::cout << "CircularBufferManager::pushing " << values_size << " samples - Number of tags found: " << tags.size() << std::endl;
        CIRCULAR_BUFFER_TYPE *buffer = static_cast<CIRCULAR_BUFFER_TYPE*>(circularBuffer_);
        if(values_size != errors_size)
            throw RUNTIME_ERROR("Error: 'values_size' needs to have the same size than 'errors_size'");
        if(!multiplexing_data_added_till_here_)
            multiplexing_data_added_till_here_ =  buffer->getCopyOfWriteIterator();
        buffer->push(values, errors, values_size);
        n_data_pushed_ += values_size;
        const std::shared_ptr<const cabad::CircularBufferBase::iterator> lastNewSample = buffer->getLatest();

        if(RTDeviceClass::getInstance()->resyncTriggerTags())
        {
            // We lost sync between the incoming WR-events and the incoming trigger tags from the Digitizer
            // Lets just skip all Trigger-Tags and WR-Events for some time and than start matching from scratch
            n_trigger_tags_ = 0;
        }
        else
        {

            bool all_new_tags_added = false;

            // If The flowgraph / scope were faster than the saftlib(DigitizerTriggerAction), we need to apply these tags in the next iteration
            if(trigger_tags_not_applied_yet_by_id_.size() > 0 )
            {
                push_trigger_tags_not_applied_yet();

                // The list usually should be empty by now .. though we allow few real late matches
                if(trigger_tags_not_applied_yet_by_id_.size() > 10)
                {
                    //if( signal_name_ == "GE01QS9D:Current_1@1kHz")
                    //    std::cout << "Dropped " << trigger_tags_not_applied_yet_by_id_.size() << " trigger tags for which no context match was found. Something is seriously broken here !!" << std::endl;
                    for(const auto& tag : trigger_tags_not_applied_yet_by_id_)
                      n_matched_triggers_.push(0.);
                    trigger_tags_not_applied_yet_by_id_.clear();
                }
            }


            all_new_tags_added = push_tags(tags);

            // If all tags were added, we can add context data until the last sample
            // If not so, we only add context data until the latest matching tag
            const  std::shared_ptr<const cabad::CircularBufferBase::iterator> first_sample_to_add_context = circularBuffer_->next(*multiplexing_data_added_till_here_);
            if(all_new_tags_added)
            {
                //addMultiplexingContextDataForRange(first_sample_to_add_context, buffer->getLatest());
                multiplexing_data_added_till_here_ = buffer->getLatest();
            }
            else
            {
                if(last_metadata_added_ && last_metadata_added_->getRelatedSample())
                {
                    //addMultiplexingContextDataForRange(first_sample_to_add_context, last_metadata_added_->getRelatedSample());
                    multiplexing_data_added_till_here_ = last_metadata_added_->getRelatedSample();
                }
            }
        }

        pushBackDataFinished();
    }
    catch(std::exception& ex)
    {
        LOG_ERROR_IF(logger, ex.what());
    }
}

void CircularBufferManager::copyDataWindow(cabad::CircularWindowIterator* window_iter, float* values, float* errors, const std::size_t& buffer_size, std::size_t& n_data_written)
{
    if(true) // TODO: Correctly cut out STREAMING Windows, according to context
    {
        CIRCULAR_BUFFER_TYPE *buffer = static_cast<CIRCULAR_BUFFER_TYPE*>(circularBuffer_);
        if(!buffer->copyDataWindow(window_iter->getDataStart(), window_iter->getDataEnd(), window_iter->getWindowValidationCheckpoint(), values, errors, buffer_size, n_data_written))
        {
            throw RUNTIME_ERROR("The " + window_iter->getName() + " read iterator got invalid while getting Data... conflict with write iterator");
        }
        //std::cout << "copyDataWindow - copied " << n_data_written << "samples" << std::endl;
    }
}

std::string CircularBufferManager::checkSampRate(float expected_sample_rate_hz, uint64_t timestamp_now_ns)
{
    int64_t time_diff = 0;
    int64_t count_diff = 0;
    std::string message;

    if(last_samp_rate_check_time_ != 0)
    {
        time_diff = timestamp_now_ns - last_samp_rate_check_time_;
        count_diff = n_data_pushed_ - last_samp_rate_check_samp_count_;
        float samp_rate_meas_hz = (1000000000. * count_diff) / time_diff;
        last_measured_samp_rates_hz_.push(samp_rate_meas_hz);
        //std::cout << "samp_rate_meas_hz    : " << samp_rate_meas_hz << std::endl;
        //std::cout << "samp_rate_expected_hz: " << expected_sample_rate << std::endl;

        float tolerance = expected_sample_rate_hz * 0.01; // 1% tolerance
        if(abs (expected_sample_rate_hz - last_measured_samp_rates_hz_.get_average()) > tolerance)
        {
            last_samp_rate_check_ok_ = false;
            message =  getSignalName() + "\t : Measured Rate: \t" + std::to_string(samp_rate_meas_hz) + " Hz.\n";
        }
        else
            last_samp_rate_check_ok_ = true;

    }
    last_samp_rate_check_time_ = timestamp_now_ns;
    last_samp_rate_check_samp_count_ = n_data_pushed_;

    return message;
}

} /* namespace DigitizerClass2 */
