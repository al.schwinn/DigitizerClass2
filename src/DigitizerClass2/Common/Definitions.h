/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#ifndef SRC_DIGITIZERCLASS2_COMMON_DEFINITIONS_H_
#define SRC_DIGITIZERCLASS2_COMMON_DEFINITIONS_H_

#include <string>
#include <exception>
#include <fesa-core-gsi/RealTime/TimingUtility.h>
#include <fesa-core/DataStore/DeviceInstantiationData.h>

#include <cabad/CircularBuffer.h>
#include <cabad/CircularBufferManagerBase.h>

#define CIRCULAR_BUFFER_TYPE cabad::CircularBufferDual<float, float>

#define RUNTIME_ERROR(x) std::runtime_error(__FILE__  + std::string(":") + std::to_string(__LINE__) + " Error: " + x )

// Nasty hack in order to pass some information to the has-data-changed method
#define NOTIFICATION_IDENTIFIER_PREFIX  "FAIR.SELECTOR.C="

#define LOGICAL_EVENT_NAME_FOR_DIGITIZER_TRIGGERS "digitizerTriggerEvent"

const std::string BLOCK_PARAM_VALUE_SEPARATOR = "|";

// Minimum amount of triggers which have to match in 'per 100' If less triggers match, a warning will be logged
#define TRIGGER_MATCHING_PERCENTAGE_MIN 95

namespace DigitizerClass2
{

const unsigned int EVENT_NO_BEAM_BP_START   = 256;
const unsigned int EVENT_NO_SEQ_START       = 257;
const unsigned int EVENT_NO_BEAM_INJECTION  = 283;
const unsigned int EVENT_NO_BEAM_EXTRACTION = 284;

} // end namespace
#endif /* SRC_DIGITIZERCLASS2_COMMON_DEFINITIONS_H_ */
