
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _DigitizerClass2_GetChannelConfigDAQ_H_
#define _DigitizerClass2_GetChannelConfigDAQ_H_

#include <DigitizerClass2/GeneratedCode/GetSetDefaultServerAction.h>
#include <DigitizerClass2/GeneratedCode/PropertyData.h>
#include <DigitizerClass2/GeneratedCode/TypeDefinition.h>
#include <DigitizerClass2/GeneratedCode/Device.h>

namespace DigitizerClass2
{

class GetChannelConfigDAQ : public GetChannelConfigDAQBase
{
public:
    GetChannelConfigDAQ(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~GetChannelConfigDAQ();
    void execute(fesa::RequestEvent* pEvt, Device* pDev, ChannelConfigDAQPropertyData& data, const ChannelConfigDAQFilterData& filter);
    bool hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const ChannelConfigDAQFilterData& filter) const;
    bool isFilterValid(const fesa::AbstractDevice& abstractDevice, const ChannelConfigDAQFilterData& filter) const;
};

} // DigitizerClass2

#endif // _DigitizerClass2_GetChannelConfigDAQ_H_
