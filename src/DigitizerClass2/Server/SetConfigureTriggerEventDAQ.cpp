
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <DigitizerClass2/Server/SetConfigureTriggerEventDAQ.h>
#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>
#include <DigitizerClass2/RealTime/RTDeviceClass.h>
#include <DigitizerClass2/Common/DeviceDataBuffer.h>

#include <cmw-log/Logger.h>

#include <climits>

namespace 
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.Server.SetConfigureTriggerEventDAQ");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "DigitizerClass2"; \
    diagMsg.name = "SetConfigureTriggerEventDAQ"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    DigitizerClass2ServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace DigitizerClass2
{

SetConfigureTriggerEventDAQ::SetConfigureTriggerEventDAQ(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
        SetConfigureTriggerEventDAQBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

SetConfigureTriggerEventDAQ::~SetConfigureTriggerEventDAQ()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that aggregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter input parameter meant to fine tune the treatment.
 */
void SetConfigureTriggerEventDAQ::execute(fesa::RequestEvent* pEvt, Device* pDev, const ConfigureTriggerEventDAQPropertyData& data, const ConfigureTriggerEventDAQFilterData& filter)
{
    RTDeviceClass* rtDeviceClass = RTDeviceClass::getInstance();

    boost::shared_ptr<DeviceDataBuffer> buffer = rtDeviceClass->getDeviceDataBuffer(pDev);
    buffer->setTriggerEventEnableState(buffer->eventName2eventID(filter.getTriggerEventName()), data.getEnabled());

    uint32_t found_at_index = std::numeric_limits<uint32_t>::max();
    uint32_t first_empty_index = std::numeric_limits<uint32_t>::max();

    // For the sake of persistancy, we store the disabled triggers as well in FESA
    uint32_t index = 0;
    uint32_t emoty_spot = 0;
    for ( auto triggerName : pDev->disabledTriggerEvents.getStrings(pEvt->getMultiplexingContext()) )
    {
        if( filter.getTriggerEventName() == triggerName)
            found_at_index = index;
        if( first_empty_index == std::numeric_limits<uint32_t>::max() && !buffer->isTriggerEvent(triggerName))
            first_empty_index = index;
       index++;
    }

    if(found_at_index == std::numeric_limits<uint32_t>::max())
    {
        // Not found and is disabled, so lets add it to the list if it got disabled
        if(!data.getEnabled())
        {
            pDev->disabledTriggerEvents.setString(filter.getTriggerEventName(), first_empty_index, pEvt->getMultiplexingContext());
            this->serviceLocator_->triggerPersistency();
        }
    }
    else
    {
        // Found, but it just got enabled. So lets remove it from list
        if(!data.getEnabled())
        {
            pDev->disabledTriggerEvents.setString("", found_at_index, pEvt->getMultiplexingContext());
            this->serviceLocator_->triggerPersistency();
        }
    }

    auto globalDev = DigitizerClass2ServiceLocator_->getGlobalDevice();
    std::string ftrnName = globalDev->ftrnDeviceName.getAsString();
    rtDeviceClass->configurePexariaIOs (pDev, ftrnName);
}

} // DigitizerClass2
