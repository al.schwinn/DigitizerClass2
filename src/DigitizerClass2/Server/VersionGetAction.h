/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#ifndef _DigitizerClass2_VersionGetAction_H_
#define _DigitizerClass2_VersionGetAction_H_

#include <DigitizerClass2/GeneratedCode/GetSetDefaultServerAction.h>
#include <DigitizerClass2/GeneratedCode/PropertyData.h>
#include <DigitizerClass2/GeneratedCode/TypeDefinition.h>
#include <DigitizerClass2/GeneratedCode/Device.h>

namespace DigitizerClass2
{

class VersionGetAction : public VersionGetActionBase
{
public:
    VersionGetAction(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~VersionGetAction();
    void execute(fesa::RequestEvent* pEvt, Device* pDev, VersionPropertyData& data, const VersionFilterData& filter);
    bool hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const VersionFilterData& filter) const;
    bool isFilterValid(const fesa::AbstractDevice& abstractDevice, const VersionFilterData& filter) const;
};

} // DigitizerClass2

#endif // _DigitizerClass2_VersionGetAction_H_
