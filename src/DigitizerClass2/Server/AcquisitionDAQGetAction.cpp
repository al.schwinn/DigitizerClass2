/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <DigitizerClass2/Server/AcquisitionDAQGetAction.h>
#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>
#include <DigitizerClass2/RealTime/RTDeviceClass.h>
#include <DigitizerClass2/Common/CabadTimingContext.h>
#include <DigitizerClass2/Common/DeviceDataBuffer.h>
#include <DigitizerClass2/Common/Sink.h>

#include <cabad/DataReadyManager.h>
#include <cabad/CircularBufferManagerBase.h>

#include <fesa-core/Utilities/StringUtilities.h>
#include <fesa-core-gsi/Synchronization/Selector.h>

#include <cmw-log/Logger.h>
#include <cmw-data/Data.h>

#define NANOSECONDS_PER_DAY 86400000000000L
#define CHANNEL_SEPARATOR ","

namespace 
{
 static uint32_t last_sequence_id = 0;
 static uint64_t last_chain_start_stamp = 0;
 cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.Server.AcquisitionDAQGetAction");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "DigitizerClass2"; \
    diagMsg.name = "AcquisitionDAQGetAction"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    DigitizerClass2ServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace DigitizerClass2
{

// Additional window-specific info which is required to build a composed window (Required in case of subscribe to multiple channels at once)
struct WindowInfo
{
    WindowInfo(CircularBufferManager *buffer_manager):
        buffer_manager_(buffer_manager)
    {

    }

    cabad::CircularWindowIterator* window_iterator_;
    CircularBufferManager* buffer_manager_;
};


AcquisitionDAQGetAction::AcquisitionDAQGetAction(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
        AcquisitionDAQGetActionBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
    local_reference_time_ = m_clock.now();
    local_reference_time_nano_ = std::chrono::duration_cast<std::chrono::nanoseconds> (local_reference_time_.time_since_epoch()).count();
}

AcquisitionDAQGetAction::~AcquisitionDAQGetAction()
{
}

uint64_t get_timestamp_nano_utc2()
{
    timespec start_time;
    clock_gettime(CLOCK_REALTIME, &start_time);
    return (start_time.tv_sec * 1000000000) + (start_time.tv_nsec);
}

cabad::MaxClientUpdateFrequencyType AcquisitionDAQGetAction::classFreqType2Frequency(STREAMING_CLIENT_UPDATE_FREQUENCY::STREAMING_CLIENT_UPDATE_FREQUENCY class_freq) const
{
    if(class_freq == STREAMING_CLIENT_UPDATE_FREQUENCY::STREAMING_CLIENT_UPDATE_FREQUENCY::FREQUENCY_1Hz )
        return 1;
    if(class_freq == STREAMING_CLIENT_UPDATE_FREQUENCY::STREAMING_CLIENT_UPDATE_FREQUENCY::FREQUENCY_10Hz )
        return 10;
    if(class_freq == STREAMING_CLIENT_UPDATE_FREQUENCY::STREAMING_CLIENT_UPDATE_FREQUENCY::FREQUENCY_25Hz )
        return 25;
    throw fesa::FesaException(__FILE__, __LINE__, "Failed to transform class specific maxClinetUpdateFrequency of '" + std::to_string(class_freq) + "' to MaxClientUpdateFrequencyType.");
}

cabad::ClientNotificationType AcquisitionDAQGetAction::classAcqMode2DAQClientNotificationType(ACQUISITION_MODE::ACQUISITION_MODE acquisition_mode) const
{
    if(acquisition_mode == ACQUISITION_MODE::STREAMING )
        return cabad::ClientNotificationType::STREAMING;
    if(acquisition_mode == ACQUISITION_MODE::FULL_SEQUENCE )
        return cabad::ClientNotificationType::FULL_SEQUENCE;
    if(acquisition_mode == ACQUISITION_MODE::TRIGGERED )
        return cabad::ClientNotificationType::TRIGGERED;
    throw fesa::FesaException(__FILE__, __LINE__, "Failed to transform class specific acquisition mode of '" + std::to_string(acquisition_mode) + "' to DAQ - ClientNotificationType.");
}

void AcquisitionDAQGetAction::fillTimeSinceRefTriggerForEachSampleLocal(WindowInfo& window_info,
                                                                        float*      timesSinceRefTrigger,
                                                                        std::size_t timesSinceRefTrigger_size)
{
    auto now = m_clock.now();
    //std::cout << "fillMetaData 2.1" << std::endl;
    uint64_t elapsed_nano = std::chrono::duration_cast<std::chrono::nanoseconds> (now - local_reference_time_ ).count();
    //printf("%.*g\n", DBL_DIG, double(elapsed_nano));
    double elapsed_sec = double(elapsed_nano) / double(1000000000);

    //printf("1. %.*g\n", DBL_DIG, elapsed_sec);
    for (std::size_t i=0;i< timesSinceRefTrigger_size;i++)
        timesSinceRefTrigger[i] = elapsed_sec + i * window_info.buffer_manager_->getSampleToSampleDistance();
}

uint64_t AcquisitionDAQGetAction::updateLocalTimestamp()
{
    auto now = m_clock.now();
    //std::cout << "fillMetaData 2.1" << std::endl;
    uint64_t elapsed_nano = std::chrono::duration_cast<std::chrono::nanoseconds> (now - local_reference_time_ ).count();
    if (elapsed_nano > NANOSECONDS_PER_DAY) //FIXME: Why to convert into nano here ?
    {
        local_reference_time_ = now;
        local_reference_time_nano_ = std::chrono::duration_cast<std::chrono::nanoseconds> (local_reference_time_.time_since_epoch()).count();
    }
}

void AcquisitionDAQGetAction::fillMetaDataLocalTiming(AcquisitionDAQPropertyData& data)
{
    data.setProcessIndex(-1);
    data.setSequenceIndex(-1);
    data.setChainIndex(-1);
    data.setEventNumber(-1);
    data.setTimingGroupID(-1);
    data.setEventStamp(-1);
    data.setProcessStartStamp(-1);
    data.setSequenceStartStamp(-1);
    data.setChainStartStamp(-1);

   // FIXME: Preleminary filled with NaN ... to be properly filled later
   data.setChannelUserDelay(std::nan(""));
   data.setChannelActualDelay(std::nan(""));
   data.setTemperature(std::nan(""));
}

void AcquisitionDAQGetAction::fillMetaDataWRTiming(Device* pDev, AcquisitionDAQPropertyData& data, cabad::CircularWindowIterator* window_iterator )
{
    std::shared_ptr<const cabad::SampleMetadata> ref_trigger = window_iterator->getRefMeta();

    if(!ref_trigger->hasTimingContext())
        throw fesa::FesaException(__FILE__, __LINE__, "Error: Reference Trigger does not hold a WREvent for unknown reason.");

     boost::shared_ptr<DeviceDataBuffer> deviceDataBuffer = RTDeviceClass::getInstance()->getDeviceDataBuffer(pDev);

     std::shared_ptr<const CabadTimingContext> timing_context;
     if( !( timing_context = std::dynamic_pointer_cast<const CabadTimingContext>(ref_trigger->getTimingContext())))
     {
         LOG_ERROR_IF(logger, "failed to cast cabad::TimingContext to CabadTimingContext");
         return;
     }
    data.setProcessIndex(timing_context->getWRContext()->getProcessIndex());
    data.setSequenceIndex(timing_context->getWRContext()->getSequenceIndex());
    data.setChainIndex(timing_context->getWRContext()->getBeamProductionChainIndex());
    data.setEventNumber(timing_context->getWRContext()->getEventNumber());
    data.setTimingGroupID(timing_context->getWRContext()->getGroupID());
    data.setEventStamp(timing_context->getWRContext()->getEventExecutionTimeStamp());
    data.setProcessStartStamp(timing_context->getWRContext()->getProcessTimeStamp());
    data.setSequenceStartStamp(timing_context->getWRContext()->getSequenceTimeStamp());

//    {
//        uint64_t now = get_timestamp_nano_utc2();
//        uint64_t nano = 1000000000;
//        now += (2 * 3600 * nano);//2h UTC offset
//        now += (37 * nano); //UTC - TAI offset
//        std::cout << "time diff 2: " << (now - ref_trigger->getWRContext()->getTimeStamp()) <<  "ns" << std::endl;
//        std::cout << "stamp2: " << ref_trigger->getWRContext()->getTimeStamp() << std::endl;
//    }

    // TODO:  Remove Hack.Hack starts here (See https://gitlab.com/al.schwinn/DigitizerClass2/issues/83)
    // Currently we just use the sequence start stamp as chain start stamp .. actually chain start stamp should be earlier (injection unilac)
    // We assume that LSA has increasing seq-ids during each chain. (workaround suggested by Hanno)
    if ( timing_context->getWRContext()->getSequenceIndex() <= last_sequence_id)
    {
        if( last_chain_start_stamp < timing_context->getWRContext()->getSequenceTimeStamp())
            last_chain_start_stamp = timing_context->getWRContext()->getSequenceTimeStamp();
    }
    last_sequence_id = timing_context->getWRContext()->getSequenceIndex();
    data.setChainStartStamp(last_chain_start_stamp);
    // Hack ends here

    // ### Fill all acquisitionContextCol value-items
    auto& metaDataCol = window_iterator->getMetaDataCol();

    // Most as C-arrays to please fesa
    std::vector <std::string> acquisitionContextCol_eventName;
    int32_t acquisitionContextCol_processIndex[MAX_ACQ_CONTEXT_COL_SIZE];
    int32_t acquisitionContextCol_sequenceIndex[MAX_ACQ_CONTEXT_COL_SIZE];
    int32_t acquisitionContextCol_chainIndex[MAX_ACQ_CONTEXT_COL_SIZE];
    int32_t acquisitionContextCol_eventNumber[MAX_ACQ_CONTEXT_COL_SIZE];
    int32_t acquisitionContextCol_timingGroupID[MAX_ACQ_CONTEXT_COL_SIZE];
    int64_t acquisitionContextCol_eventStamp[MAX_ACQ_CONTEXT_COL_SIZE];
    int64_t acquisitionContextCol_processStartStamp[MAX_ACQ_CONTEXT_COL_SIZE];
    int64_t acquisitionContextCol_sequenceStartStamp[MAX_ACQ_CONTEXT_COL_SIZE];
    int64_t acquisitionContextCol_chainStartStamp[MAX_ACQ_CONTEXT_COL_SIZE];
    int8_t  acquisitionContextCol_eventFlags[MAX_ACQ_CONTEXT_COL_SIZE];
    int16_t acquisitionContextCol_reserved[MAX_ACQ_CONTEXT_COL_SIZE];
    int64_t acquisitionContextCol_event_id_raw[MAX_ACQ_CONTEXT_COL_SIZE];
    int64_t acquisitionContextCol_param_raw[MAX_ACQ_CONTEXT_COL_SIZE];

    int n_wr_events_found = 0;
    for(auto& metaData : metaDataCol)
    {
        if(metaData->hasTimingContext())
        {
            std::shared_ptr<const CabadTimingContext> cabad_timing_context;
            if( !( cabad_timing_context = std::dynamic_pointer_cast<const CabadTimingContext>(metaData->getTimingContext())))
            {
                LOG_ERROR_IF(logger, "failed to cast cabad::TimingContext to CabadTimingContext");
                return;
            }
            boost::shared_ptr<fesaGSI::TimingContextWR> wr_context = cabad_timing_context->getWRContext();
            acquisitionContextCol_eventNumber[n_wr_events_found] = wr_context->getEventNumber();
            acquisitionContextCol_eventName.push_back(deviceDataBuffer->eventID2eventName(wr_context->getEventNumber()));
            acquisitionContextCol_processIndex[n_wr_events_found] = wr_context->getProcessIndex();
            acquisitionContextCol_sequenceIndex[n_wr_events_found] = wr_context->getSequenceIndex();
            acquisitionContextCol_chainIndex[n_wr_events_found] = wr_context->getPayload().beamProductionChainIndex;
            acquisitionContextCol_timingGroupID[n_wr_events_found] = wr_context->getGroupID();
            acquisitionContextCol_eventStamp[n_wr_events_found] = wr_context->getEventExecutionTimeStamp();
            acquisitionContextCol_processStartStamp[n_wr_events_found] = wr_context->getProcessTimeStamp();
            acquisitionContextCol_sequenceStartStamp[n_wr_events_found] = wr_context->getSequenceTimeStamp();
            acquisitionContextCol_chainStartStamp[n_wr_events_found] = wr_context->getPayload().beamProductionChainTimestamp;
            acquisitionContextCol_eventFlags[n_wr_events_found] = wr_context->getEventFlags();
            acquisitionContextCol_reserved[n_wr_events_found] = wr_context->getReserved();
            acquisitionContextCol_event_id_raw[n_wr_events_found] = 0; // To be filled as soon as FESA provides the raw values
            acquisitionContextCol_param_raw[n_wr_events_found] = 0 ;   // To be filled as soon as FESA provides the raw values

            n_wr_events_found++;
        }
    }
    if( n_wr_events_found > 0)
    {
        data.setAcquisitionContextCol_eventName(acquisitionContextCol_eventName);
        data.setAcquisitionContextCol_processIndex(&acquisitionContextCol_processIndex[0], n_wr_events_found);
        data.setAcquisitionContextCol_sequenceIndex(&acquisitionContextCol_sequenceIndex[0], n_wr_events_found);
        data.setAcquisitionContextCol_chainIndex(acquisitionContextCol_chainIndex, n_wr_events_found);
        data.setAcquisitionContextCol_eventNumber(acquisitionContextCol_eventNumber, n_wr_events_found);
        data.setAcquisitionContextCol_timingGroupID(acquisitionContextCol_timingGroupID, n_wr_events_found);
        data.setAcquisitionContextCol_eventStamp(acquisitionContextCol_eventStamp, n_wr_events_found);
        data.setAcquisitionContextCol_processStartStamp(acquisitionContextCol_processStartStamp, n_wr_events_found);
        data.setAcquisitionContextCol_sequenceStartStamp(acquisitionContextCol_sequenceStartStamp, n_wr_events_found);
        data.setAcquisitionContextCol_chainStartStamp(acquisitionContextCol_chainStartStamp, n_wr_events_found);
        data.setAcquisitionContextCol_eventFlags(acquisitionContextCol_eventFlags, n_wr_events_found);
        data.setAcquisitionContextCol_reserved(acquisitionContextCol_reserved, n_wr_events_found);
        data.setAcquisitionContextCol_event_id_raw(acquisitionContextCol_event_id_raw, n_wr_events_found);
        data.setAcquisitionContextCol_param_raw(acquisitionContextCol_param_raw, n_wr_events_found);
    }
    // ### End Fill all acquisitionContextCol value-items

    // FIXME: Preleminary filled with NaN ... to be properly filled later
    data.setChannelUserDelay(std::nan(""));
    data.setChannelActualDelay(std::nan(""));
    data.setTemperature(std::nan(""));
}


/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that aggregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter input parameter meant to fine tune the treatment.
 */
void AcquisitionDAQGetAction::execute(fesa::RequestEvent* pEvt, Device* pDev, AcquisitionDAQPropertyData& data, const AcquisitionDAQFilterData& filter)
{
    NoneContext context;
    bool allocated_1D_arrays = false;
    bool allocated_2D_arrays = false;

    std::size_t channelIndex = 0;

    // fesa only allows to write the whole 2D array at once (as 1D array and n_rows/n_columns) .. so we are forced to copy
    std::size_t data_iter = 0;

    // as soon as we know the number of channels, these c-arrays will be allocated
    int64_t* status;
    float* channelRangeMin;
    float* channelRangeMax;

    // as soon as we know the window size, values/errors will be allocated for all channels
    float* values;
    float* errors;
    float* channelTimeSinceRefTriggers;

    try
    {
        RTDeviceClass* rtDeviceClass = RTDeviceClass::getInstance();

        //std::cout << "AcquisitionDAQGetAction::execute 0 "  << std::endl;
        if( data.getUpdateType() == fesa::UpdateType::UpdateType::FIRST )
        {
            // Note that Fesa 7.x occassionally provides the wrong UpdateType. See: https://www-acc.gsi.de/bugzilla/show_bug.cgi?id=1816
            // So the Warning might be displayed by accident
            // Should be fixed in fesa 8.x (than we can use "return" in that case)
            LOG_WARNING_IF(logger, "FIRST update is not supported. Please disable it in the file fesa.cfg !");
        }

        std::string channelNameFilter = filter.getChannelNameFilter();
        fesa::StringUtilities::trimWhiteSpace(channelNameFilter);
        std::vector<std::string> channelNames;
        std::vector<std::string> channelUnits;
        fesa::StringUtilities::tokenize(channelNameFilter, channelNames, CHANNEL_SEPARATOR);

        fesaGSI::Selector selector(pEvt->getMultiplexingContext()->getExtraCondition());
        uint32_t notificationDataID = selector.getChainIndex(); // nasty hack, used to transfere NotificationData-ID from RT to Server
        cabad::ClientNotificationData notificationData;
        rtDeviceClass->getDAQDataReadyManager()->getDataByID(notificationDataID, notificationData);

        std::vector<WindowInfo> window_info_col;
        // First get the window information we need for each channel
        for(auto it = channelNames.begin(); it != channelNames.end();)
        {
            auto channelName = *it;

            auto sinkIt = std::find(notificationData.sinkNames_.begin(), notificationData.sinkNames_.end(), channelName);
            if (sinkIt == notificationData.sinkNames_.end())
            {
                LOG_WARNING_IF(logger, "Did not find channel " + channelName + " in the notification data.");
                it = channelNames.erase(it);
                continue;
            }

            auto index = std::distance(notificationData.sinkNames_.begin(), sinkIt);
            if (static_cast<std::size_t>(index) >= notificationData.windows_.size())
            {
                throw fesa::FesaException(__FILE__, __LINE__, "Invalid index. Expected windows size equal to sinkNames size." );
            }

            Sink *sink = rtDeviceClass->getSink(pDev, channelName);
            channelUnits.push_back(sink->getUnit());
            WindowInfo window_info(sink->getCircularBufferManager());
            window_info.window_iterator_ = &notificationData.windows_[index];
            window_info_col.push_back(std::move(window_info));
            ++it;
        }

        if(window_info_col.empty())
            throw fesa::FesaException(__FILE__, __LINE__, "No windows found which match the provided channel names." );

        status = new int64_t[channelNames.size()];
        channelRangeMin = new float[channelNames.size()];
        channelRangeMax = new float[channelNames.size()];
        allocated_1D_arrays = true;
        data.setChannelNames(channelNames);

        // Make sure that all channels share the same ref trigger stamp
        std::shared_ptr<const cabad::SampleMetadata> ref_trigger = window_info_col[0].window_iterator_->getRefMeta();
        for(auto &window_info : window_info_col)
        {
           /* If both refer to a WR-Event, check if they refer to the same WR-Event */
           if(ref_trigger
              && window_info.window_iterator_->getRefMeta()
              && window_info.window_iterator_->getRefMeta()->hasTimingContext()
              && ref_trigger->hasTimingContext())
            {
               if(window_info.window_iterator_->getRefMeta()->getTimingContext() != ref_trigger->getTimingContext())
               {
                   std::string message = "Channels to notify dont share the same reference trigger";
                   LOG_ERROR_IF(logger, message);
                   throw fesa::FesaException(__FILE__, __LINE__, message);
               }
            }
           ref_trigger = window_info.window_iterator_->getRefMeta();
        }
        // Set the ref trigger info
        if (ref_trigger)
        {
            boost::shared_ptr<DeviceDataBuffer> deviceDataBuffer = RTDeviceClass::getInstance()->getDeviceDataBuffer(pDev);
            data.setRefTriggerName(deviceDataBuffer->eventID2eventName(ref_trigger->getTimingContext()->getEventNumber()));
            data.setRefTriggerStamp(ref_trigger->getTimingContext()->getTimeStamp());
            data.setAcquisitionStamp(ref_trigger->getTimingContext()->getTimeStamp());
        }
        else
        {
            updateLocalTimestamp();
            data.setRefTriggerName(STRING_NO_REF_TRIGGER_FOUND);
            data.setRefTriggerStamp(local_reference_time_nano_);
            data.setAcquisitionStamp(local_reference_time_nano_);
        }

        // Calculate the size we need to the composed window
        std::size_t max_window_size = 0;
        for(auto &window_info : window_info_col)
        {
            if(window_info.window_iterator_->windowSize() > max_window_size)
                max_window_size = window_info.window_iterator_->windowSize();
        }

        // Allocate memory for all buffers, accoring to biggest required window
        //std::cout << "AcquisitionDAQGetAction::execute --Window Size: " << max_window_size << std::endl;
        values = new float[max_window_size * window_info_col.size()];
        errors = new float[max_window_size * window_info_col.size()];
        channelTimeSinceRefTriggers = new float[max_window_size * window_info_col.size()];
        allocated_2D_arrays = true;

        // Copy the data
        std::size_t n_data_written = 0;
        for(auto &window_info : window_info_col)
        {
            std::size_t window_size = window_info.window_iterator_->windowSize();
            window_info.buffer_manager_->copyDataWindow(window_info.window_iterator_,
                                                         &values[data_iter],
                                                         &errors[data_iter],
                                                         window_size,
                                                         n_data_written);
            // Write timeSinceRefTrigger data
            if (ref_trigger)
            {
                window_info.window_iterator_->fillTimeSinceRefMetaForEachSample(&channelTimeSinceRefTriggers[data_iter], window_size);
                fillMetaDataWRTiming(pDev, data, window_info.window_iterator_);
            }
            else
            {
                fillTimeSinceRefTriggerForEachSampleLocal(window_info, &channelTimeSinceRefTriggers[data_iter], window_size);
                fillMetaDataLocalTiming(data);
            }

            if (n_data_written != window_size)
                throw fesa::FesaException(__FILE__, __LINE__, "Some data could not be copied, aborting.");

            data_iter += n_data_written;

            // fill remaining slots wit nan, if required
            for(std::size_t i = n_data_written; i < max_window_size; i++)
            {
                values[data_iter] = std::nan("");
                errors[data_iter] = std::nan("");
                channelTimeSinceRefTriggers[data_iter] = std::nan("");
                data_iter ++;
            }

            status[channelIndex] = window_info.window_iterator_->getWindowStatus();
            if (!window_info.buffer_manager_->lastSampRateCheckOk())
                status[channelIndex] |= 1UL << 12;

            // TODO: Fill with meaningfull data
            channelRangeMax[channelIndex] = std::nan("");
            channelRangeMin[channelIndex] = std::nan("");

            channelIndex++;

        }

        data.setChannelUnit(channelUnits);

        //std::cout << "AcquisitionDAQGetAction::execute 5 "  << std::endl;
        // This will transfere ownership of the 2D- arrays to cmw (cmw will free the memory when finished) ... that prevents an additional copy
        cmw::data::Data& cmw_data = const_cast<cmw::data::Data&>(data.getCMWData());
        cmw_data.appendArrayMove("status",                       status,                      channelNames.size());
        cmw_data.appendArrayMove("channelRangeMin",              channelRangeMin,             channelNames.size());
        cmw_data.appendArrayMove("channelRangeMax",              channelRangeMax,             channelNames.size());
        cmw_data.appendArray2DMove("channelValues",              values,                      channelNames.size(), max_window_size);
        cmw_data.appendArray2DMove("channelErrors",              errors,                      channelNames.size(), max_window_size);
        cmw_data.appendArray2DMove("channelTimeSinceRefTrigger", channelTimeSinceRefTriggers, channelNames.size(), max_window_size);

        //std::cout << "AcquisitionDAQGetAction::execute 6 "  << std::endl;


    }
    catch(std::exception &ex)
    {
        //std::cout << "AcquisitionDAQGetAction::Ex1"  << std::endl;
        if(allocated_1D_arrays)
        {
            delete[] status;
            delete[] channelRangeMin;
            delete[] channelRangeMax;
        }
        if(allocated_2D_arrays)
        {
            delete[] values;
            delete[] errors;
            delete[] channelTimeSinceRefTriggers;
        }
        LOG_ERROR_IF(logger, ex.what());
        throw;
    }
    catch(...)
    {
        //std::cout << "AcquisitionDAQGetAction::Ex2"  << std::endl;
        if(allocated_1D_arrays)
        {
            delete[] status;
            delete[] channelRangeMin;
            delete[] channelRangeMax;
        }
        if(allocated_2D_arrays)
        {
            delete[] values;
            delete[] errors;
            delete[] channelTimeSinceRefTriggers;
        }
        LOG_ERROR_IF(logger, "Unknown exception caught in AcquisitionDAQGetAction::execute");
        throw;
    }
    //std::cout << "AcquisitionDAQGetAction::execute 7 "  << std::endl;
}

// This method is called when a new subscription is requested. Returning false will abort the subscription. (But it is much better to throw, since like that the client knows what is wrong)
bool AcquisitionDAQGetAction::isFilterValid(const fesa::AbstractDevice& abstractDevice, const AcquisitionDAQFilterData& filter) const
{
    const Device& device = static_cast<const Device&>(abstractDevice);

    RTDeviceClass* rtDeviceClass = RTDeviceClass::getInstance();

    if( !filter.isAcquisitionModeFilterAvailable())
        throw fesa::FesaException(__FILE__, __LINE__, "AcquisitionModeFilter not Available");

    if( filter.getAcquisitionModeFilter() == ACQUISITION_MODE::SNAPSHOT )
        throw fesa::FesaException(__FILE__, __LINE__, "SNAPSHOT mode not yet supported");

    if( filter.getAcquisitionModeFilter() == ACQUISITION_MODE::POST_MORTEM )
        throw fesa::FesaException(__FILE__, __LINE__, "POST_MORTEM mode not yet supported");

    if( !filter.isChannelNameFilterAvailable() || filter.getChannelNameFilterAsString().empty())
        throw fesa::FesaException(__FILE__, __LINE__, "ChannelNameFilter not Available");

    // TODO: Check if Client update frequency <= sink update frequency
//    if( filter.getMaxClientUpdateFrequencyFilter() != STREAMING_CLIENT_UPDATE_FREQUENCY::STREAMING_CLIENT_UPDATE_FREQUENCY::FREQUENCY_1Hz   )
//        throw fesa::FesaException(__FILE__, __LINE__, "Currently only update Frequency of 1Hz supported");

    //This will throw an exception if the channelName is unknown
    std::string channelNameFilter = filter.getChannelNameFilter();
    fesa::StringUtilities::trimWhiteSpace(channelNameFilter);
    std::vector<std::string> channelNames;
    fesa::StringUtilities::tokenize(channelNameFilter, channelNames, CHANNEL_SEPARATOR);
    for(auto& channelName : channelNames)
    {
        Sink *sink = rtDeviceClass->getSink(&device, channelName);
        if( sink->getSinkMode() == SinkMode::Triggered && filter.getAcquisitionModeFilter() != ACQUISITION_MODE::TRIGGERED )
            throw fesa::FesaException(__FILE__, __LINE__, "Triggered Sinks only can be used in acquisition mode TRIGGERED");

        if( sink->getSinkMode() == SinkMode::Streaming && filter.getAcquisitionModeFilter() != ACQUISITION_MODE::STREAMING &&  filter.getAcquisitionModeFilter() != ACQUISITION_MODE::FULL_SEQUENCE && filter.getAcquisitionModeFilter() != ACQUISITION_MODE::SNAPSHOT)
            throw fesa::FesaException(__FILE__, __LINE__, "Streaming Sinks only can be used in acquisition modes STREAMING, FULL_SEQUENCE and SNAPSHOT");

        // FIXME: Temporary only FULL_SEQUENCE for channels > 25Hz
        if(filter.getAcquisitionModeFilter() == ACQUISITION_MODE::FULL_SEQUENCE && sink->getSampleRate() <= 25.)
        {
            throw fesa::FesaException(__FILE__, __LINE__, "Sorry, due to a bug currently FULL_SEQ mode is only available for channels with a sampling rate > 25Hz ! ... working on it.");
        }
    }

    if( filter.isTriggerNameFilterAvailable() && !filter.getTriggerNameFilterAsString().empty())
    {
        if(filter.getAcquisitionModeFilter() != ACQUISITION_MODE::TRIGGERED)
        {
            throw fesa::FesaException(__FILE__, __LINE__, "Filtering for specific triggers only supported in acquisition mode TRIGGERED");
        }
    }

    cabad::MaxClientUpdateFrequencyType updateFrequency_hz = 1; // default, if no filter is specified
    if( filter.isMaxClientUpdateFrequencyFilterAvailable() )
        updateFrequency_hz = classFreqType2Frequency(filter.getMaxClientUpdateFrequencyFilter());
    if( channelNames.size() > 1)
    {
        if(rtDeviceClass->getDAQDataReadyManager()->doTheseCircularBuffersShareTheSameDataReadyHandler(channelNames,
                                                classAcqMode2DAQClientNotificationType(filter.getAcquisitionModeFilter()),
                                                updateFrequency_hz) == false)
        {
            throw fesa::FesaException(__FILE__, __LINE__, "All specified channels need to share the same sample rate, notification type and update frequency !");
        }
    }

    return true;
}

// Use this member function to tell the framework whether the subscribed clients need to be updated or not. See the FESA wiki for more info
bool AcquisitionDAQGetAction::hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const AcquisitionDAQFilterData& filter) const
{
   // std::cout << "AcquisitionDAQGetAction::hasDataChanged 1 "  << std::endl;
    //std::cout << "event.getMultiplexingContext()->getExtraCondition(): " << event.getMultiplexingContext()->getExtraCondition()  << std::endl;

    Device& device = static_cast<Device&>(abstractDevice);
    RTDeviceClass* rtDeviceClass = RTDeviceClass::getInstance();
    fesaGSI::Selector selector(event.getMultiplexingContext()->getExtraCondition());
    uint32_t data_id = selector.getChainIndex(); // nasty hack, used to transfere NotificationData-ID from RT to Server
    cabad::ClientNotificationData notificationData;
    bool found = rtDeviceClass->getDAQDataReadyManager()->getDataByID(data_id, notificationData);
    if(!found)
        throw fesa::FesaException(__FILE__, __LINE__, "Failed to find notificationData for data_id: " + std::to_string(data_id));

    if( event.getMultiplexingContext()->getExtraCondition().find(NOTIFICATION_IDENTIFIER_PREFIX) == std::string::npos)
        throw fesa::FesaException(__FILE__, __LINE__, "NOTIFICATION_IDENTIFIER_PREFIX not found");

    std::string channelNameFilter = filter.getChannelNameFilter();
    fesa::StringUtilities::trimWhiteSpace(channelNameFilter);
    std::vector<std::string> channelNames;
    fesa::StringUtilities::tokenize(channelNameFilter, channelNames, CHANNEL_SEPARATOR);

    if( notificationData.notificationType_ == cabad::ClientNotificationType::STREAMING )
    {
        if( filter.getAcquisitionModeFilter() != ACQUISITION_MODE::STREAMING  )
            return false;
        cabad::MaxClientUpdateFrequencyType updateFrequency_hz = 1; // default, if no filter is specified
        if( filter.isMaxClientUpdateFrequencyFilterAvailable() )
            updateFrequency_hz = classFreqType2Frequency(filter.getMaxClientUpdateFrequencyFilter());
        if(notificationData.updateFrequency_ == updateFrequency_hz)
        {
           // std::cout << "AcquisitionDAQGetAction::hasDataChanged 2 "  << std::endl;
            // do subscribed channels match the sinknames of this notification ?
            for (auto& channelName : channelNames)
            {
                if (std::find(notificationData.sinkNames_.begin(), notificationData.sinkNames_.end(), channelName) == notificationData.sinkNames_.end())
                    return false;
            }
            return true;
        }
    }
    else if( notificationData.notificationType_ == cabad::ClientNotificationType::FULL_SEQUENCE )
    {
        if ( filter.getAcquisitionModeFilter() != ACQUISITION_MODE::FULL_SEQUENCE )
            return false;

        if( !notificationData.context_ )
            throw fesa::FesaException(__FILE__, __LINE__, "Logical Error: FULL_SEQUENCE update without context received");

        if( notificationData.context_->getEventNumber() != EVENT_NO_SEQ_START )
            return false;
        //std::cout << "AcquisitionDAQGetAction::hasDataChanged - Full SEQ"<< std::endl;

        // do subscribed channels match the sinknames of this notification ?
        for (auto& channelName : channelNames)
        {
            if (std::find(notificationData.sinkNames_.begin(), notificationData.sinkNames_.end(), channelName) == notificationData.sinkNames_.end())
                return false;
        }
        return true;
    }
    else if( notificationData.notificationType_ == cabad::ClientNotificationType::TRIGGERED )
    {
        if ( filter.getAcquisitionModeFilter() != ACQUISITION_MODE::TRIGGERED )
            return false;

        if( !notificationData.context_ )
            throw fesa::FesaException(__FILE__, __LINE__, "Logical Error: TRIGGERED update without context received");

        std::string triggerName = rtDeviceClass->getDeviceDataBuffer(&device)->eventID2eventName(notificationData.context_->getEventNumber());

        //std::cout << "AcquisitionDAQGetAction::hasDataChanged - Triggered" << std::endl;

        // do subscribed channels match the sinknames of this notification ?
        for (auto& channelName : channelNames)
        {
            if (std::find(notificationData.sinkNames_.begin(), notificationData.sinkNames_.end(), channelName) == notificationData.sinkNames_.end())
                return false;
        }
        if( !filter.isTriggerNameFilterAvailable() || filter.getTriggerNameFilterAsString().empty() )
            return true;
        if( filter.getTriggerNameFilter() == triggerName )
            return true;
        return false;
    }
    else
        throw fesa::FesaException(__FILE__, __LINE__, "Unknown Action Type: " + std::to_string(cabad::as_integer(notificationData.notificationType_)));

    return false;
}

} // DigitizerClass2
