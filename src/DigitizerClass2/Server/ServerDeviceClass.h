/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#ifndef _DigitizerClass2_SERVER_DEVICE_CLASS_H_
#define _DigitizerClass2_SERVER_DEVICE_CLASS_H_

#include <DigitizerClass2/GeneratedCode/ServerDeviceClassGen.h>

namespace DigitizerClass2
{

class ServerDeviceClass: public ServerDeviceClassGen
{
    public:
        static ServerDeviceClass* getInstance();
        static void releaseInstance();
        void specificInit();
        void specificShutDown();
    private:
        ServerDeviceClass();
        virtual  ~ServerDeviceClass();
        static ServerDeviceClass* instance_;
};

} // DigitizerClass2

#endif // _DigitizerClass2_SERVER_DEVICE_CLASS_H_
