/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <DigitizerClass2/Server/VersionGetAction.h>
#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

#include <digitizers/constants.h>
#include <flowgraph/constants.h>

namespace 
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.Server.VersionGetAction");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "DigitizerClass2"; \
    diagMsg.name = "VersionGetAction"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    DigitizerClass2ServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace DigitizerClass2
{

VersionGetAction::VersionGetAction(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
        VersionGetActionBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

VersionGetAction::~VersionGetAction()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that aggregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter input parameter meant to fine tune the treatment.
 */
void VersionGetAction::execute(fesa::RequestEvent* pEvt, Device* pDev, VersionPropertyData& data, const VersionFilterData& filter)
{
    data.setGr_digitizer_version(digitizers::version());
    data.setGr_flowgraph_version(flowgraph::version());
    data.setDaqAPIVersion("2.0");
}

// Use this member function to tell the framework whether the subscribed clients need to be updated or not. See the FESA wiki for more info
bool VersionGetAction::hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const VersionFilterData& filter) const
{
    //Device& device = static_cast<Device&>(abstractDevice);
    return true;
}

// This method is called when a new subscription is requested. Returning false will abort the subscription.
bool VersionGetAction::isFilterValid(const fesa::AbstractDevice& abstractDevice, const VersionFilterData& filter) const
{
    //Device& device = static_cast<Device&>(abstractDevice);
    return true;
}

} // DigitizerClass2
