
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _DigitizerClass2_SetFlowgraphParameter_H_
#define _DigitizerClass2_SetFlowgraphParameter_H_

#include <DigitizerClass2/GeneratedCode/GetSetDefaultServerAction.h>
#include <DigitizerClass2/GeneratedCode/PropertyData.h>
#include <DigitizerClass2/GeneratedCode/TypeDefinition.h>
#include <DigitizerClass2/GeneratedCode/Device.h>

namespace DigitizerClass2
{

class SetFlowGraphParameter : public SetFlowGraphParameterBase
{
public:
    SetFlowGraphParameter(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~SetFlowGraphParameter();
    void execute(fesa::RequestEvent* pEvt, Device* pDev, const FlowGraphParameterPropertyData& data, const FlowGraphParameterFilterData& filter);
};

} // DigitizerClass2

#endif // _DigitizerClass2_SetFlowgraphParameter_H_
