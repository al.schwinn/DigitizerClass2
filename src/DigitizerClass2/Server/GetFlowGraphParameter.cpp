
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include "GetFlowGraphParameter.h"

#include <DigitizerClass2/Common/FlowGraphParser.h>
#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>
// #include <regex> only for gcc >=4.9.0

#include <cmw-log/Logger.h>

namespace 
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.Server.GetFlowGraphParameter");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "DigitizerClass2"; \
    diagMsg.name = "GetFlowgraphParameter"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    DigitizerClass2ServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace DigitizerClass2
{

GetFlowGraphParameter::GetFlowGraphParameter(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
        GetFlowGraphParameterBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

GetFlowGraphParameter::~GetFlowGraphParameter()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that aggregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter input parameter meant to fine tune the treatment.
 */
void GetFlowGraphParameter::execute(fesa::RequestEvent* pEvt, Device* pDev, FlowGraphParameterPropertyData& data, const FlowGraphParameterFilterData& filter)
{
    std::string flowGraph_string = pDev->flowGraph.getAsString(pEvt->getMultiplexingContext());
    FlowGraphParser parser(flowGraph_string);


    std::vector<std::string> parameterValuesFlowGraph;
    std::vector<std::string> parameterValuesOut;
    parser.document_to_param_value_tokens(parameterValuesFlowGraph);

    if(filter.isParameterFiltersAvailable() == false )
    {
        // no filter set ? --> return everything !
        data.setParameterValues(parameterValuesFlowGraph);
        return;
    }
    std::vector<std::string> expressions(filter.getParameterFiltersAsStrings());
    if(expressions.size() == 0)
    {
        // no filter set ? --> return everything !
        data.setParameterValues(parameterValuesFlowGraph);
        return;
    }

    for(auto parameterValue : parameterValuesFlowGraph)
    {
        for(auto expr: expressions)
        {
            if (expr.empty() || parameterValue.find(expr) != std::string::npos)
            {
                parameterValuesOut.push_back(parameterValue);
                break;
            }
        }
    }
    data.setParameterValues(parameterValuesOut);
}

// Use this member function to tell the framework whether the subscribed clients need to be updated or not. See the FESA wiki for more info
bool GetFlowGraphParameter::hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const FlowGraphParameterFilterData& filter) const
{
    //Device& device = static_cast<Device&>(abstractDevice);
    return true;
}

// This method is called when a new subscription is requested. Returning false will abort the subscription.
bool GetFlowGraphParameter::isFilterValid(const fesa::AbstractDevice& abstractDevice, const FlowGraphParameterFilterData& filter) const
{
    //Device& device = static_cast<Device&>(abstractDevice);
    return true;
}

} // DigitizerClass2
