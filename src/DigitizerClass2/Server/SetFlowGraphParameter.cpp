
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <DigitizerClass2/Server/SetFlowGraphParameter.h>
#include <DigitizerClass2/Common/FlowGraphParser.h>
#include <DigitizerClass2/Common/Definitions.h>
#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

namespace 
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.Server.SetFlowGraphParameter");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "DigitizerClass2"; \
    diagMsg.name = "SetFlowGraphParameter"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    DigitizerClass2ServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace DigitizerClass2
{

SetFlowGraphParameter::SetFlowGraphParameter(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
        SetFlowGraphParameterBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

SetFlowGraphParameter::~SetFlowGraphParameter()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that aggregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter input parameter meant to fine tune the treatment.
 */
void SetFlowGraphParameter::execute(fesa::RequestEvent* pEvt, Device* pDev, const FlowGraphParameterPropertyData& data, const FlowGraphParameterFilterData& filter)
{
    std::string flowGraph_string = pDev->flowGraph.getAsString(pEvt->getMultiplexingContext());
    FlowGraphParser parser(flowGraph_string);

    std::vector<std::string> expressions;
    if(filter.isParameterFiltersAvailable())
        expressions = filter.getParameterFiltersAsStrings();
    if(expressions.size() == 0)
    {
        // no filter set ? --> Add "all" filter, so that everything is matched
        expressions.push_back("");
    }

    std::vector<std::string> parameterValues = data.getParameterValuesAsStrings();
    std::size_t separator_size = BLOCK_PARAM_VALUE_SEPARATOR.size();

    for(auto parameterValue : parameterValues)
    {
        for(auto expr: expressions)
        {
            if (expr.empty() || parameterValue.find(expr) != std::string::npos)
            {
                std::size_t pos1 = parameterValue.find(BLOCK_PARAM_VALUE_SEPARATOR);
                std::string message = "Error: Expected format: <Block>" + BLOCK_PARAM_VALUE_SEPARATOR + "<Parameter>" + BLOCK_PARAM_VALUE_SEPARATOR + "<Value>,<Block>" + BLOCK_PARAM_VALUE_SEPARATOR + "<Parameter>" + BLOCK_PARAM_VALUE_SEPARATOR + "<Value>,...";
                if (pos1==std::string::npos)
                    throw fesa::FesaException(__FILE__, __LINE__, message);
                std::size_t pos2 = parameterValue.find(BLOCK_PARAM_VALUE_SEPARATOR, pos1+1);
                if (pos2==std::string::npos)
                    throw fesa::FesaException(__FILE__, __LINE__, message);

                std::string block = parameterValue.substr(0, pos1);
                std::string param = parameterValue.substr(pos1+separator_size, pos2-pos1-separator_size);
                std::string value = parameterValue.substr(pos2+separator_size);
        //        std::cout << "block: " << block <<std::endl;
        //        std::cout << "param: " << param << std::endl;
        //        std::cout << "value: " << value << std::endl;
                parser.writeGrBlockParameter(block, param, value);
                break;
            }
        }
    }

    pDev->flowGraph.set(parser.document_to_string().c_str(),pEvt->getMultiplexingContext());

    // Persist flowgraph and restart FESA class
    AbstractAction::triggerOnDemandEventSource("PersistAndResetSource",pEvt->getMultiplexingContext());
}

} // DigitizerClass2
