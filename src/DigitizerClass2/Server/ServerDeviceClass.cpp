/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#include <DigitizerClass2/Server/ServerDeviceClass.h>
#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>

#include <cmw-log/Logger.h>

#include <string>
#include <vector>

namespace
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.Server.ServerDeviceClass");

} // namespace

namespace DigitizerClass2
{

ServerDeviceClass* ServerDeviceClass::instance_ = NULL;

ServerDeviceClass::ServerDeviceClass () :
    ServerDeviceClassGen()
{
}

ServerDeviceClass::~ServerDeviceClass()
{
}

ServerDeviceClass* ServerDeviceClass::getInstance()
{
    if (instance_ == NULL)
    {
        instance_ = new ServerDeviceClass();
    }
    return instance_;
}

void ServerDeviceClass::releaseInstance()
{
    if (instance_ != NULL) 
    {
        delete instance_;
        instance_ = NULL;
    }
}

// This method is called when the FESA class starts up.
// You can write code that initializes devices in the loop below.
void ServerDeviceClass::specificInit()
{
    const Devices& deviceCol = DigitizerClass2ServiceLocator_->getDeviceCollection();
    for (Devices::const_iterator it = deviceCol.begin(); it != deviceCol.end(); ++it)
    {
        try
        {
            Device* device = *it;
            static_cast<void>(device); // This line prevents an "unused variable" warning, it can be removed safely.
            // Write here some code to initialize devices
            
        }
        catch (const fesa::FesaException& exception)
        {
            LOG_ERROR_IF(logger, exception.getMessage());
            // Re-throwing the exception prevents the process from starting up.
            throw;
        }
    }
}

void ServerDeviceClass::specificShutDown()
{
    // This method is executed just before a normal shut down of the process.
}

} // DigitizerClass2
