
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <DigitizerClass2/Server/SetChannelLogLevel.h>
#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>
#include <DigitizerClass2/RealTime/RTDeviceClass.h>
#include <DigitizerClass2/Common/CircularBufferManager.h>
#include <DigitizerClass2/Common/Sink.h>

#include <cmw-log/Logger.h>

#include <cabad/Definitions.h>

namespace 
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.Server.SetChannelLogLevel");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "DigitizerClass2"; \
    diagMsg.name = "SetChannelLogLevel"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    DigitizerClass2ServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace DigitizerClass2
{

SetChannelLogLevel::SetChannelLogLevel(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
        SetChannelLogLevelBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

SetChannelLogLevel::~SetChannelLogLevel()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that aggregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter input parameter meant to fine tune the treatment.
 */
void SetChannelLogLevel::execute(fesa::RequestEvent* pEvt, Device* pDev, const ChannelLogLevelPropertyData& data, const ChannelLogLevelFilterData& filter)
{
    RTDeviceClass* rtDeviceClass = RTDeviceClass::getInstance();
    rtDeviceClass->forAllSinks([&](Sink* sink) {
            CircularBufferManager* bufferManager = dynamic_cast<CircularBufferManager*>(sink->getCircularBufferManager());

            if(bufferManager->getSignalName() == filter.getChannelName())
                {
                    if(filter.getAcquisitionMode() == ACQUISITION_MODE::STREAMING)
                        bufferManager->setLogLevelStreaming(cabad::LogLevel(data.getLogLevel()), filter.getMaxClientUpdateFrequency());
                    else if (filter.getAcquisitionMode() == ACQUISITION_MODE::TRIGGERED)
                        bufferManager->setLogLevelTriggered(cabad::LogLevel(data.getLogLevel()));
                    else if (filter.getAcquisitionMode() == ACQUISITION_MODE::FULL_SEQUENCE)
                        bufferManager->setLogLevelFullSequence(cabad::LogLevel(data.getLogLevel()));
                    else
                        throw fesa::FesaException(__FILE__, __LINE__, "unknown Acquisition Mode");
                }
    });
}

} // DigitizerClass2
