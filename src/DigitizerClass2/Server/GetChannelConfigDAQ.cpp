
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <DigitizerClass2/Server/GetChannelConfigDAQ.h>
#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>

#include <DigitizerClass2/RealTime/RTDeviceClass.h>
#include <DigitizerClass2/Common/DeviceDataBuffer.h>
#include <DigitizerClass2/Common/Sink.h>

#include <cmw-log/Logger.h>

namespace 
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.Server.GetChannelConfigDAQ");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "DigitizerClass2"; \
    diagMsg.name = "GetChannelConfigDAQ"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    DigitizerClass2ServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace DigitizerClass2
{

#define STATUS_LABEL_SIZE 13
struct status
{
    std::string                                 label;
    DAQ_STATUS_SEVERITY::DAQ_STATUS_SEVERITY    severity;
};

GetChannelConfigDAQ::GetChannelConfigDAQ(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
        GetChannelConfigDAQBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

GetChannelConfigDAQ::~GetChannelConfigDAQ()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that aggregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter input parameter meant to fine tune the treatment.
 */
void GetChannelConfigDAQ::execute(fesa::RequestEvent* pEvt, Device* pDev, ChannelConfigDAQPropertyData& data, const ChannelConfigDAQFilterData& filter)
{
    RTDeviceClass* rtDeviceClass = RTDeviceClass::getInstance();
    boost::shared_ptr<DeviceDataBuffer> buffer = rtDeviceClass->getDeviceDataBuffer(pDev);
    std::vector <std::string> channelNames;
    std::vector <std::string> channelUnits;
    float                     channelDataRates[MAX_NUMBER_OF_CHANNELS];
    int                       i = 0;

    buffer->forAllSinks([&](Sink* sink) {
        channelNames.push_back(sink->getSignalName());
        channelUnits.push_back(sink->getUnit());
        channelDataRates[i] = sink->getSampleRate();
        i++;
    });

    std::vector<status> status_labels_severities {
          {"OVERVOLTAGE"           ,DAQ_STATUS_SEVERITY::ERROR},
          {"REALIGNMENT_FAILED"    ,DAQ_STATUS_SEVERITY::ERROR},
          {"SAMPLES_LOST"          ,DAQ_STATUS_SEVERITY::ERROR},
          {"MAX_OVERFLOW_ERROR"    ,DAQ_STATUS_SEVERITY::ERROR},
          {"MAX_OVERFLOW_WARNING"  ,DAQ_STATUS_SEVERITY::WARNING},
          {"MAX_OVERFLOW_INFO"     ,DAQ_STATUS_SEVERITY::INFO},
          {"MIN_OVERFLOW_ERROR"    ,DAQ_STATUS_SEVERITY::ERROR},
          {"MIN_OVERFLOW_WARNING"  ,DAQ_STATUS_SEVERITY::WARNING},
          {"MIN_OVERFLOW_INFO"     ,DAQ_STATUS_SEVERITY::INFO},
          {"UNDERFLOW_ERROR"       ,DAQ_STATUS_SEVERITY::ERROR},
          {"UNDERFLOW_WARNING"     ,DAQ_STATUS_SEVERITY::WARNING},
          {"UNDERFLOW_INFO"        ,DAQ_STATUS_SEVERITY::INFO},
          {"HW_SAMPLE_RATE_ERROR"  ,DAQ_STATUS_SEVERITY::ERROR}
        };

    assert(STATUS_LABEL_SIZE == status_labels_severities.size());
    DAQ_STATUS_SEVERITY::DAQ_STATUS_SEVERITY status_severities[STATUS_LABEL_SIZE];
    std::vector<std::string> status_labels;

     for (int i=0;i< STATUS_LABEL_SIZE;++i)
     {
         status_labels.push_back(status_labels_severities[i].label);
         status_severities[i] = status_labels_severities[i].severity;
     }
    data.setStatus_labels(status_labels);
    data.setStatus_severity(status_severities, STATUS_LABEL_SIZE);

    DeviceDataBuffer::TriggerEventsByEventIDType events = buffer->getTriggerEvents();
    std::vector <std::string> eventNames;
    bool eventEnableStates[100]; // C-array to please fesa
    i = 0;
    for (auto &event : events)
    {
        eventNames.push_back(event.second.name);
        eventEnableStates[i] = event.second.enabled;
        i++;
    }
    data.setChannelNames(channelNames);
    data.setChannelUnits(channelUnits);
    data.setTriggerEvents(eventNames);
    data.setTriggerEventsEnabled(&eventEnableStates[0], events.size());
    data.setChannelDataRates(&channelDataRates[0], i);
}

// Use this member function to tell the framework whether the subscribed clients need to be updated or not. See the FESA wiki for more info
bool GetChannelConfigDAQ::hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const ChannelConfigDAQFilterData& filter) const
{
    //Device& device = static_cast<Device&>(abstractDevice);
    return true;
}

// This method is called when a new subscription is requested. Returning false will abort the subscription.
bool GetChannelConfigDAQ::isFilterValid(const fesa::AbstractDevice& abstractDevice, const ChannelConfigDAQFilterData& filter) const
{
    //Device& device = static_cast<Device&>(abstractDevice);
    return true;
}

} // DigitizerClass2
