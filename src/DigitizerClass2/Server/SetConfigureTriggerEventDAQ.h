
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _DigitizerClass2_SetConfigureTriggerEventDAQ_H_
#define _DigitizerClass2_SetConfigureTriggerEventDAQ_H_

#include <DigitizerClass2/GeneratedCode/GetSetDefaultServerAction.h>
#include <DigitizerClass2/GeneratedCode/PropertyData.h>
#include <DigitizerClass2/GeneratedCode/TypeDefinition.h>
#include <DigitizerClass2/GeneratedCode/Device.h>

namespace DigitizerClass2
{

class SetConfigureTriggerEventDAQ : public SetConfigureTriggerEventDAQBase
{
public:
    SetConfigureTriggerEventDAQ(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~SetConfigureTriggerEventDAQ();
    void execute(fesa::RequestEvent* pEvt, Device* pDev, const ConfigureTriggerEventDAQPropertyData& data, const ConfigureTriggerEventDAQFilterData& filter);
};

} // DigitizerClass2

#endif // _DigitizerClass2_SetConfigureTriggerEventDAQ_H_
