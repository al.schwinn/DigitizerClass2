
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#ifndef _DigitizerClass2_GetFlowgraphParameter_H_
#define _DigitizerClass2_GetFlowgraphParameter_H_

#include <DigitizerClass2/GeneratedCode/GetSetDefaultServerAction.h>
#include <DigitizerClass2/GeneratedCode/PropertyData.h>
#include <DigitizerClass2/GeneratedCode/TypeDefinition.h>
#include <DigitizerClass2/GeneratedCode/Device.h>

namespace DigitizerClass2
{

class GetFlowGraphParameter : public GetFlowGraphParameterBase
{
public:
    GetFlowGraphParameter(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~GetFlowGraphParameter();
    void execute(fesa::RequestEvent* pEvt, Device* pDev, FlowGraphParameterPropertyData& data, const FlowGraphParameterFilterData& filter);
    bool hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const FlowGraphParameterFilterData& filter) const;
    bool isFilterValid(const fesa::AbstractDevice& abstractDevice, const FlowGraphParameterFilterData& filter) const;
};

} // DigitizerClass2

#endif // _DigitizerClass2_GetFlowgraphParameter_H_
