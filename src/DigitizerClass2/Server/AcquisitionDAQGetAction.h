/* Copyright (C) 2020 GSI Darmstadt, Germany - All Rights Reserved
 * This software is licensed under the GNU Affero General Public License version 3 (see the file LICENSE). */

#ifndef _DigitizerClass2_AcquisitionDAQGetAction_H_
#define _DigitizerClass2_AcquisitionDAQGetAction_H_

#include <DigitizerClass2/GeneratedCode/GetSetDefaultServerAction.h>
#include <DigitizerClass2/GeneratedCode/PropertyData.h>
#include <DigitizerClass2/GeneratedCode/TypeDefinition.h>
#include <DigitizerClass2/GeneratedCode/Device.h>
#include <DigitizerClass2/Common/Definitions.h>
#include <DigitizerClass2/Common/CircularBufferManager.h>

#include <cabad/CircularBufferManagerBase.h>
#include <cabad/CircularWindowIterator.h>

#include <chrono>

namespace DigitizerClass2
{
class Sink;
class WindowInfo;

class AcquisitionDAQGetAction : public AcquisitionDAQGetActionBase
{
public:
    AcquisitionDAQGetAction(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses);
    virtual ~AcquisitionDAQGetAction();
    void execute(fesa::RequestEvent* pEvt, Device* pDev, AcquisitionDAQPropertyData& data, const AcquisitionDAQFilterData& filter);
    bool hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const AcquisitionDAQFilterData& filter) const;

    bool isFilterValid(const fesa::AbstractDevice& abstractDevice, const AcquisitionDAQFilterData& filter) const;
private:
    cabad::MaxClientUpdateFrequencyType classFreqType2Frequency(STREAMING_CLIENT_UPDATE_FREQUENCY::STREAMING_CLIENT_UPDATE_FREQUENCY class_freq) const;
    cabad::ClientNotificationType classAcqMode2DAQClientNotificationType(ACQUISITION_MODE::ACQUISITION_MODE acquisition_mode) const;

    void fillTimeSinceRefTriggerForEachSampleLocal(WindowInfo& window_info,
                                                   float*      timesSinceRefTrigger,
                                                   std::size_t timesSinceRefTrigger_size);
    uint64_t updateLocalTimestamp();
    void fillMetaDataLocalTiming(AcquisitionDAQPropertyData& data);
    void fillMetaDataWRTiming(Device* pDev, AcquisitionDAQPropertyData& data, cabad::CircularWindowIterator* window_iterator);

   std::chrono::high_resolution_clock m_clock;
   std::chrono::time_point<std::chrono::high_resolution_clock> local_reference_time_;
   int64_t local_reference_time_nano_;
};

} // DigitizerClass2

#endif // _DigitizerClass2_AcquisitionDAQGetAction_H_
