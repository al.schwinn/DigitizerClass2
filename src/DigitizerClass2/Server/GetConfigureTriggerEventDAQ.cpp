
// FESA framework 
// Use this code as a starting point to develop your own equipment class 

#include <DigitizerClass2/Server/GetConfigureTriggerEventDAQ.h>
#include <DigitizerClass2/GeneratedCode/ServiceLocator.h>
#include <DigitizerClass2/RealTime/RTDeviceClass.h>
#include <DigitizerClass2/Common/DeviceDataBuffer.h>

#include <cmw-log/Logger.h>

namespace 
{

cmw::log::Logger& logger = cmw::log::LoggerFactory::getLogger("FESA.USR.DigitizerClass2.Server.GetConfigureTriggerEventDAQ");

} // namespace

#define LOG_DIAG_IF(topic,message) \
{ \
    DiagnosticsDefs::DiagnosticMessage diagMsg(DiagnosticsDefs::Side::user, DiagnosticsDefs::Source::rt); \
    diagMsg.fesaClass = "DigitizerClass2"; \
    diagMsg.name = "GetConfigureTriggerEventDAQ"; \
    diagMsg.action = DiagnosticsDefs::Action::undefined; \
    diagMsg.msg = message; \
    DigitizerClass2ServiceLocator_->logDiagnosticMessage(topic, diagMsg); \
}

#define LOG_DIAG_DEVICE_IF(topic,message,device) \
{ \
    if (device->isLoggable()) \
    { \
        LOG_DIAG_IF(topic,message); \
    } \
}

namespace DigitizerClass2
{

GetConfigureTriggerEventDAQ::GetConfigureTriggerEventDAQ(fesa::ServerActionConfig& actionConfig, const fesa::AbstractServiceLocator* serviceLocator, const std::map<std::string, const fesa::AbstractServiceLocator*>& serviceLocatorRelatedClasses):
        GetConfigureTriggerEventDAQBase(actionConfig, serviceLocator, serviceLocatorRelatedClasses)
{
}

GetConfigureTriggerEventDAQ::~GetConfigureTriggerEventDAQ()
{
}

/*!
 * \brief This method is executed consecutively to a GET/SET action:
 * \param pEvt requestEvent from which the multiplexingContext can be extracted
 * \param pDev points to the Device or Global instance involved by this request 
 * \param data composite structure that aggregates one or several types data-entries corresponding to the property definition.
 * This data object is the input (resp. output) which is passed when invoking a Get (resp. Set). 
 * \param filter input parameter meant to fine tune the treatment.
 */
void GetConfigureTriggerEventDAQ::execute(fesa::RequestEvent* pEvt, Device* pDev, ConfigureTriggerEventDAQPropertyData& data, const ConfigureTriggerEventDAQFilterData& filter)
{
    RTDeviceClass* rtDeviceClass = RTDeviceClass::getInstance();
    boost::shared_ptr<DeviceDataBuffer> buffer = rtDeviceClass->getDeviceDataBuffer(pDev);

    bool enabled = buffer->isTriggerEventEnabled(buffer->eventName2eventID(filter.getTriggerEventName()));
    data.setEnabled(enabled);
}

// Use this member function to tell the framework whether the subscribed clients need to be updated or not. See the FESA wiki for more info
bool GetConfigureTriggerEventDAQ::hasDataChanged(const fesa::RequestEvent& event, fesa::AbstractDevice& abstractDevice, const ConfigureTriggerEventDAQFilterData& filter) const
{
    //Device& device = static_cast<Device&>(abstractDevice);
    return true;
}

// This method is called when a new subscription is requested. Returning false will abort the subscription.
bool GetConfigureTriggerEventDAQ::isFilterValid(const fesa::AbstractDevice& abstractDevice, const ConfigureTriggerEventDAQFilterData& filter) const
{
    //Device& device = static_cast<Device&>(abstractDevice);
    return true;
}

} // DigitizerClass2
